﻿//#define USING_SHARED_RATE_LIMITS

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Nancy;

namespace FIORest;

internal class IgnoredRepeatMiddleware
{
    private class RequestSignature
    {
        string Path;
        string Source;
        string Method;
        byte[] Body;
        int Hash;

        public RequestSignature(string path, string source, string method, byte[] body)
        {
            Path = path;
            Source = source;
            Method = method;
#if USING_SHARED_RATE_LIMITS
            if (IsSharedRateLimitEndpoint(path, method))
            {
                Source = "SHARED_ENDPOINT";
            }
#endif // USING_SHARED_RATE_LIMITS
            Body = body;
            HashCode hc = new HashCode();
            hc.Add(path);
            hc.Add(method);
            hc.Add(source);
            hc.AddBytes(body);
            Hash = hc.ToHashCode();
        }

        private static bool IsSharedRateLimitEndpoint(string path, string method)
        {
            return SharedRateLimitAttribute.SharedRateLimits.Contains($"{method.ToUpper()}-{path.ToUpper()}");
        }

        // Overload equals to compare fields
        public bool Equals(RequestSignature other)
        {
            if (other == null)
                return false;
            if (ReferenceEquals(this, other))
                return true;

            if (Path != other.Path)
                return false;
            if (Source != other.Source)
                return false;
            if (Method != other.Method)
                return false;
            if (Body.SequenceEqual(other.Body) == false)
                return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            RequestSignature sig = obj as RequestSignature;
            if (sig == null)
                return false;
            else
                return Equals(sig);
        }

        public static bool operator ==(RequestSignature lhs, RequestSignature rhs)
        {
            if (((object)lhs) == null || ((object)rhs) == null)
                return Object.Equals(lhs, rhs);

            return lhs.Equals(rhs);
        }

        public static bool operator !=(RequestSignature lhs, RequestSignature rhs)
        {
            if (((object)lhs) == null || ((object)rhs) == null)
                return !Object.Equals(lhs, rhs);

            return !(lhs.Equals(rhs));
        }

        public override int GetHashCode()
        {
            return Hash;
        }
    }

    static ConcurrentDictionary<RequestSignature, DateTime> requests = new();
    private const string XForwardedForHeaderkey = "X-Forwarded-For";

    static public async Task<Response> Invoke(NancyContext context, CancellationToken token)
    {
        var body = context.Request.Body;
        long origPosition = body.Position;
        byte[] data = new byte[body.Length];
        body.Read(data, (int)body.Position, (int)(body.Length - body.Position));
        body.Position = origPosition;

        var now = DateTime.Now;
        // Try and keep the request cache small by removing older items.
        var oldRequests = requests.Where((item) => item.Value < now);
        foreach(var req in oldRequests)
        {
            requests.Remove(req.Key, out _);
        }
        // Go find/create this match.
        var nextExpiry = now.AddMilliseconds(1100);
        string source = context.Request.UserHostAddress;
        if (context.Request.Headers.Keys.Contains(XForwardedForHeaderkey))
        {
            source = String.Join(";", context.Request.Headers[XForwardedForHeaderkey]);
        }
        var sig = new RequestSignature(context.Request.Path, context.Request.UserHostAddress, context.Request.Method, data);
        DateTime expires = requests.GetOrAdd(sig, nextExpiry);
        if (expires < now)
        {
            requests.Remove(sig, out expires);
            expires = requests.GetOrAdd(sig, nextExpiry);
        }
        if (expires != nextExpiry)
        {
            var ignoreResponse = new Response();
            ignoreResponse.StatusCode = HttpStatusCode.TooManyRequests;
            return ignoreResponse;
        }

        await Task.CompletedTask;
        return null;
    }
}
