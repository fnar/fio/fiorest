﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FIORest.Database;
using FIORest.Database.Models;

using Microsoft.EntityFrameworkCore;

namespace FIORest.Caches
{
    public static class APIKeyCache
    {
        private static MRUCache<Guid, APIKey> MRUCache = new MRUCache<Guid, APIKey>(512);

        public static APIKey Get(Guid Key)
        {
            return MRUCache.Get(Key);
        }

        public static void Set(Guid Key, APIKey Value)
        {
            MRUCache.Set(Key, Value);
        }

        public static bool Remove(Guid Key)
        {
            return MRUCache.Remove(Key);
        }

        public static APIKey GetOrCache(Guid Key, PRUNDataContext context = null)
        {
            var res = Get(Key);
            if (res == null)
            {
                res = Cache(Key, context);
            }

            return res;
        }

        public static APIKey Cache(Guid APIKeyGuid, PRUNDataContext context = null)
        {
            PRUNDataContext DB = context ?? PRUNDataContext.GetNewContext();
            try
            {
                var apiKey = DB.APIKeys
                .AsNoTracking()
                .Include(ak => ak.AuthenticationModel)
                .Where(ak => ak.AuthAPIKey == APIKeyGuid)
                .FirstOrDefault();
                if (apiKey != null)
                {
                    Set(APIKeyGuid, apiKey);
                }

                return apiKey;
            }
            finally
            {
                if (context == null)
                {
                    DB.Dispose();
                }
            }
        }
    }
}
