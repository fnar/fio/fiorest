﻿#if WITH_MODULES
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace FIORest.Modules
{
    [SharedRateLimit("/chat/data", Method.POST)]
    [SharedRateLimit("/chat/message_added_self", Method.POST)]
    [SharedRateLimit("/chat/message_added", Method.POST)]
    [SharedRateLimit("/chat/message_deleted", Method.POST)]
    [SharedRateLimit("/chat/user_joined", Method.POST)]
    [SharedRateLimit("/chat/user_left", Method.POST)]
    public class ChatModule : NancyModule
    {
        private static ConcurrentDictionary<string, long> ChannelIdToMostRecentTimestamp = new();

        private const int DeleteMRUCacheSize = 50;
        private static ConcurrentDictionary<string, MRUCache<long, string>> ChannelIdToDeletedMessages = new();

        public ChatModule() : base("/chat")
        {
            Post("/data", _ =>
            {
                this.EnforceWriteAuth();
                return PostData();
            });

            Post("/message_added_self", _ =>
            {
                this.EnforceWriteAuth();
                return PostMessageAddedSelf();
            });

            Post("/message_added", _ =>
            {
                this.EnforceWriteAuth();
                return PostMessageAdded();
            });

            Post("/message_deleted", _ =>
            {
                this.EnforceWriteAuth();
                return PostMessageDeleted();
            });

            Post("/message_list", _ =>
            {
                this.EnforceWriteAuth();
                return PostMessageList();
            });

            Post("/user_joined", _ =>
            {
                this.EnforceWriteAuth();
                return PostUserJoined();
            });

            Post("/user_left", _ =>
            {
                this.EnforceWriteAuth();
                return PostUserLeft();
            });

            Post("/clear", _ =>
            {
                this.EnforceAuthAdmin();
                return PostClear();
            });

            Get("/list", _ =>
            {
                return GetList();
            });

            Get("/messages", _ =>
            {
                return GetMessages(this.Request.Query);
            });

            Get("/display/{channel_id}", parameters =>
            {
                var top = this.Request.Query["top"];
                if (top == null)
                {
                    top = 300;
                }
                if (top > 10000)
                {
                    top = 10000;
                }
                var skip = this.Request.Query["skip"];
                if (skip == null)
                {
                    skip = 0;
                }
                return GetDisplay(parameters.channel_id, top, skip);
            });

            Get("/display/pretty/{channel_id}", parameters =>
            {
                var top = this.Request.Query["top"];
                if (top == null)
                {
                    top = 300;
                }
                if (top > 10000)
                {
                    top = 10000;
                }
                var skip = this.Request.Query["skip"];
                if (skip == null)
                {
                    skip = 0;
                }
                return GetPrettyDisplay(parameters.channel_id, top, skip);
            });

            Get("/user/{username}/{channel}", parameters =>
            {
                return GetChatForUserInChannel(parameters.username, parameters.channel);
            });

            Get("/activechat", parameters =>
            {
                return GetActiveChatCount();
            });

            Get("/activechatnoleavejoin", parameters =>
            {
                return GetActiveChatCountNoLeaveJoin();
            });
        }

        private Response PostData()
        {
            using (var req = new FIORequest<JSONRepresentations.Channel.Data.Rootobject>(Request, FIOAPIEndpoint:"/chat/channel"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload.message.payload;
                if (payload != null && (payload.type == "PUBLIC" || (payload.type == "GROUP" && payload.displayName.EndsWith("Global Site Owners"))) && payload.displayName != null)
                {
                    ChatModel model = new ChatModel();

                    model.ChannelId = payload.channelId;
                    model.Type = payload.type;
                    model.NaturalId = payload.naturalId;
                    model.DisplayName = payload.displayName;
                    model.CreationTime = payload.creationTime.timestamp;
                    model.LastActivity = payload.lastActivity.timestamp;
                    model.UserCount = payload.userCount;

                    model.UserNameSubmitted = req.UserName;
                    model.Timestamp = req.Now;

                    model.Validate();

                    req.DB.ChatModels.Upsert(model)
                        .On(cm => new { cm.ChannelId })
                        .WhenMatched((existModel, newModel) => new ChatModel
                        {
                            LastActivity = newModel.LastActivity,
                            UserCount = newModel.UserCount,
                            UserNameSubmitted = newModel.UserNameSubmitted,
                            Timestamp = newModel.Timestamp
                        })
                        .Run();
                }

                return HttpStatusCode.OK;
            }
        }

        private bool ShouldUpdateMessage(ChatMessage chatMessage)
        {
            bool bShouldUpdate = false;
            if (chatMessage.MessageDeleted)
            {
                MRUCache<long, string> DeleteMRUCache;
                if (!ChannelIdToDeletedMessages.TryGetValue(chatMessage.ChatModelId, out DeleteMRUCache))
                {
                    DeleteMRUCache = new(DeleteMRUCacheSize);
                    DeleteMRUCache.Set(chatMessage.MessageTimestamp, chatMessage.MessageId);
                    ChannelIdToDeletedMessages[chatMessage.ChatModelId] = DeleteMRUCache;
                    bShouldUpdate = true;
                }
                else
                {
                    bShouldUpdate = DeleteMRUCache.Get(chatMessage.MessageTimestamp) == null;
                    if (bShouldUpdate)
                    {
                        DeleteMRUCache.Set(chatMessage.MessageTimestamp, chatMessage.MessageId);
                    }
                }
            }
            else
            {
                bShouldUpdate = !ChannelIdToMostRecentTimestamp.TryGetValue(chatMessage.ChatModelId, out var mostRecentTimestamp) || chatMessage.MessageTimestamp > mostRecentTimestamp;
                if (bShouldUpdate)
                {
                    ChannelIdToMostRecentTimestamp[chatMessage.ChatModelId] = chatMessage.MessageTimestamp;
                }
            }

            return bShouldUpdate;
        }

        private List<ChatMessage> GetMessagesToUpdate(List<ChatMessage> chatMessages)
        {
            using var pf = new PerfTracer("GetMessagesToUpdate");
            if (chatMessages == null || chatMessages.Count == 0)
            {
                return chatMessages;
            }

            System.Diagnostics.Debug.Assert(chatMessages.Select(cm => cm.ChatModelId).Distinct().Count() == 1);
            var ChatModelId = chatMessages[0].ChatModelId;

            ChannelIdToMostRecentTimestamp.GetOrAdd(ChatModelId, 0);

            // Handle regular messages
            var RegularMessages = chatMessages.Where(cm => cm.MessageDeleted == false);
            if (ChannelIdToMostRecentTimestamp.TryGetValue(ChatModelId, out var mostRecentTimestamp))
            {
                RegularMessages = RegularMessages.Where(rm => rm.MessageTimestamp > mostRecentTimestamp);       // Remove any messages that are less than the latest timestamp
                if (RegularMessages.Any())
                {
                    ChannelIdToMostRecentTimestamp[ChatModelId] = RegularMessages.Max(rm => rm.MessageTimestamp);	// Update the max timestamp
                }
            }

            // Handle deletes
            var DeleteMessages = chatMessages.Where(cm => cm.MessageDeleted == true).OrderBy(cm => cm.MessageTimestamp);
            MRUCache<long, string> DeleteMRUCache;
            if (!ChannelIdToDeletedMessages.TryGetValue(ChatModelId, out DeleteMRUCache))
            {
                DeleteMRUCache = new(DeleteMRUCacheSize);
                DeleteMRUCache.SetRange(DeleteMessages.Select(dm => new Tuple<long, string>(dm.MessageTimestamp, dm.MessageId)).ToList());
                ChannelIdToDeletedMessages[ChatModelId] = DeleteMRUCache;
            }
            else
            {
                lock (DeleteMRUCache.LockObj)
                {
                    DeleteMessages = DeleteMessages
                        .Where(dm => DeleteMRUCache.Get(dm.MessageTimestamp) == null)
                        .OrderBy(a => 1); // No-op sort, basically makes it so we get an OrderedEnumerable back
                }
            }

            return RegularMessages.Concat(DeleteMessages).ToList();
        }

        private Response PostMessageAdded()
        {
            using (var req = new FIORequest<JSONRepresentations.Channel.MessageAdded.Rootobject>(Request, FIOAPIEndpoint:"/chat/messageadded"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                if (payload != null)
                {
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.MessageId = payload.messageId;
                    chatMessage.ChatModelId = payload.channelId;

                    chatMessage.MessageType = payload.type;

                    chatMessage.SenderId = payload.sender.id;
                    chatMessage.UserName = payload.sender.username;
                    chatMessage.MessageText = payload.message;
                    chatMessage.MessageTimestamp = payload.time.timestamp;
                    chatMessage.MessageDeleted = (payload.deletingUser != null);

                    chatMessage.UserNameSubmitted = req.UserName;
                    chatMessage.Timestamp = req.Now;

                    chatMessage.Validate();

                    bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
                    if (bShouldUpdate)
                    {
                        /* 
						 * Getting fancy with this upsert.  On existing message
						 * we only want to update parts of it.
						 * And only update the submitter/time if the message was deleted.
						 */
                        req.DB.ChatMessages.Upsert(chatMessage)
                            .On(cm => new { cm.MessageId })
                            .WhenMatched((existMsg, newMsg) => new ChatMessage
                            {
                                MessageType = newMsg.MessageType,
                                MessageText = newMsg.MessageText,
                                MessageDeleted = newMsg.MessageDeleted,
                                UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
                                Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
                            })
                            .Run();
                    }
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostMessageAddedSelf()
        {
            using (var req = new FIORequest<JSONRepresentations.Channel.MessageAddedSelf.Rootobject>(Request, FIOAPIEndpoint:"/chat/messageaddedself"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload.message.payload;
                if (payload != null)
                {
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.MessageId = payload.messageId;
                    chatMessage.ChatModelId = payload.channelId;

                    chatMessage.MessageType = payload.type;

                    chatMessage.SenderId = payload.sender.id;
                    chatMessage.UserName = payload.sender.username;
                    chatMessage.MessageText = payload.message;
                    chatMessage.MessageTimestamp = payload.time.timestamp;
                    chatMessage.MessageDeleted = (payload.deletingUser != null);

                    chatMessage.UserNameSubmitted = req.UserName;
                    chatMessage.Timestamp = req.Now;

                    chatMessage.Validate();

                    bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
                    if (bShouldUpdate)
                    {
                        /* 
                         * Getting fancy with this upsert.  On existing message
                         * we only want to update parts of it.
                         * And only update the submitter/time if the message was deleted.
                         */
                        req.DB.ChatMessages.Upsert(chatMessage)
                            .On(cm => new { cm.MessageId })
                            .WhenMatched((existMsg, newMsg) => new ChatMessage
                            {
                                MessageType = newMsg.MessageType,
                                MessageText = newMsg.MessageText,
                                MessageDeleted = newMsg.MessageDeleted,
                                UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
                                Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
                            })
                            .Run();
                    }

                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostMessageDeleted()
        {
            using (var req = new FIORequest<JSONRepresentations.Channel.MessageDeleted.Rootobject>(Request, FIOAPIEndpoint:"/chat/messagedeleted"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                if (payload != null)
                {
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.MessageId = payload.messageId;
                    chatMessage.ChatModelId = payload.channelId;

                    chatMessage.MessageType = payload.type;

                    chatMessage.SenderId = payload.sender.id;
                    chatMessage.UserName = payload.sender.username;
                    chatMessage.MessageText = payload.message;
                    chatMessage.MessageTimestamp = payload.time.timestamp;
                    chatMessage.MessageDeleted = true; // Note, this varies from other implementations!

                    chatMessage.UserNameSubmitted = req.UserName;
                    chatMessage.Timestamp = req.Now;

                    chatMessage.Validate();

                    bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
                    if (bShouldUpdate)
                    {
                        /* 
                         * Getting fancy with this upsert.  On existing message
                         * we only want to update parts of it.
                         * And only update the submitter/time if the message was deleted.
                         */
                        req.DB.ChatMessages.Upsert(chatMessage)
                        .On(cm => new { cm.MessageId })
                        .WhenMatched((existMsg, newMsg) => new ChatMessage
                        {
                            MessageType = newMsg.MessageType,
                            MessageText = newMsg.MessageText,
                            MessageDeleted = newMsg.MessageDeleted,
                            UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
                            Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
                        })
                        .Run();
                    }
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostMessageList()
        {
            using (var req = new FIORequest<JSONRepresentations.Channel.MessageList.Rootobject>(Request, FIOAPIEndpoint:"/chat/messagelist"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload.message.payload;
                if (payload != null)
                {
                    ChatModel model = req.DB.ChatModels.Where(cm => cm.ChannelId == payload.channelId).FirstOrDefault();
                    if (model != null)
                    {
                        List<ChatMessage> messages = new();
                        foreach (var message in payload.messages)
                        {
                            ChatMessage chatMessage = new ChatMessage();

                            chatMessage.MessageId = message.messageId;
                            chatMessage.ChatModelId = model.ChannelId;

                            chatMessage.MessageType = message.type;

                            chatMessage.SenderId = message.sender != null ? message.sender.id : "UnknownUserId";
                            chatMessage.UserName = message.sender != null ? message.sender.username : "UnknownUserName";
                            chatMessage.MessageText = message.message;
                            chatMessage.MessageTimestamp = message.time.timestamp;
                            chatMessage.MessageDeleted = (message.deletingUser != null);

                            chatMessage.UserNameSubmitted = req.UserName;
                            chatMessage.Timestamp = req.Now;

                            chatMessage.Validate();

                            messages.Add(chatMessage);
                        }

                        messages = GetMessagesToUpdate(messages);
                        if (messages.Count > 0)
                        {
                            /* 
                             * Getting fancy with this upsert.  On existing message
                             * we only want to update parts of it.
                             * And only update the submitter/time if the message was deleted.
                             */
                            req.DB.ChatMessages.UpsertRange(messages)
                                .On(cm => new { cm.MessageId })
                                .WhenMatched((existMsg, newMsg) => new ChatMessage
                                {
                                    MessageType = newMsg.MessageType,
                                    MessageText = newMsg.MessageText,
                                    MessageDeleted = newMsg.MessageDeleted,
                                    UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
                                    Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
                                })
                                .Run();
                        }

                        req.DB.SaveChanges();
                    }
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostUserJoined()
        {
            using (var req = new FIORequest<JSONRepresentations.Channel.UserJoined.Rootobject>(Request, FIOAPIEndpoint:"/chat/userjoined"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                if (payload != null)
                {
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.MessageId = payload.messageId;
                    chatMessage.ChatModelId = payload.channelId;

                    chatMessage.MessageType = "JOINED";

                    chatMessage.SenderId = payload.user != null && payload.user.user != null ? payload.user.user.id : "UnknownUserId";
                    chatMessage.UserName = payload.user != null && payload.user.user != null ? payload.user.user.username : "UnknownUserName";
                    chatMessage.MessageText = "";
                    chatMessage.MessageTimestamp = payload.time.timestamp;
                    chatMessage.MessageDeleted = false;

                    chatMessage.UserNameSubmitted = req.UserName;
                    chatMessage.Timestamp = req.Now;

                    chatMessage.Validate();

                    bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
                    if (bShouldUpdate)
                    {
                        req.DB.ChatMessages.Upsert(chatMessage)
                        .On(cm => new { cm.MessageId })
                        .WhenMatched((existMsg, newMsg) => new ChatMessage
                        {
                            MessageType = newMsg.MessageType,
                            MessageText = newMsg.MessageText,
                            UserNameSubmitted = newMsg.UserNameSubmitted,
                            Timestamp = newMsg.Timestamp
                        })
                        .Run();
                    }

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostUserLeft()
        {
            using (var req = new FIORequest<JSONRepresentations.Channel.UserLeft.Rootobject>(Request, FIOAPIEndpoint:"/chat/userleft"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                if (payload != null)
                {
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.MessageId = payload.messageId;
                    chatMessage.ChatModelId = payload.channelId;

                    chatMessage.MessageType = "LEFT";

                    chatMessage.SenderId = payload.user != null && payload.user.user != null ? payload.user.user.id : "UnknownUserId";
                    chatMessage.UserName = payload.user != null && payload.user.user != null ? payload.user.user.username : "UnknownUserName";
                    chatMessage.MessageText = "";
                    chatMessage.MessageTimestamp = payload.time.timestamp;
                    chatMessage.MessageDeleted = false;

                    chatMessage.UserNameSubmitted = req.UserName;
                    chatMessage.Timestamp = req.Now;

                    chatMessage.Validate();

                    bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
                    if (bShouldUpdate)
                    {
                        req.DB.ChatMessages.Upsert(chatMessage)
                        .On(cm => new { cm.MessageId })
                        .WhenMatched((existMsg, newMsg) => new ChatMessage
                        {
                            MessageType = newMsg.MessageType,
                            MessageText = newMsg.MessageText,
                            UserNameSubmitted = newMsg.UserNameSubmitted,
                            Timestamp = newMsg.Timestamp
                        })
                        .Run();
                        req.DB.SaveChanges();
                    }
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostClear()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                var all = from c in DB.ChatModels select c;
                DB.ChatModels.RemoveRange(all);
                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private class ChatListing
        {
            public string DisplayName { get; set; }
            public string ChannelId { get; set; }
            public string NaturalId { get; set; }
        }

        private Response GetList()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.ChatModels
                    .AsNoTracking()
                    .Where(cm => cm.DisplayName != null)
                    .Select(cm => new ChatListing
                    {
                        DisplayName = cm.DisplayName,
                        ChannelId = cm.ChannelId,
                        NaturalId = cm.NaturalId
                    }).ToList();

                return JsonConvert.SerializeObject(res);
            }
        }

        private ChatModel GetChatModelFromChannelIdWithMessages(PRUNDataContext DB, string channelId, int topXMessages, int skipXMessages)
        {
            channelId = channelId.ToUpper();

            var model = DB.ChatModels
                .AsNoTracking()
                .Where(cm => cm.ChannelId.ToUpper() == channelId && cm.DisplayName != null)
                .Include(cm => cm.Messages.OrderByDescending(msg => msg.MessageTimestamp).Skip(skipXMessages).Take(topXMessages))
                .AsSplitQuery()
                .FirstOrDefault();
            if (model != null)
            {
                return model;
            }
            else
            {
                string candidateDisplayName = "INVALID";
                if (channelId == "GLOBAL")
                {
                    candidateDisplayName = "APEX Global Chat".ToUpper();
                }
                else if (channelId == "HELP")
                {
                    candidateDisplayName = "Official APEX Help Channel".ToUpper();
                }
                else if (channelId == "UFO")
                {
                    candidateDisplayName = "United Faction Operations".ToUpper();
                }
                else
                {
                    Match m = Regex.Match(channelId, @"\w{2}\-\d{3}\w", RegexOptions.IgnoreCase);
                    if (m.Success)
                    {
                        var planet = DB.PlanetDataModels.Where(pdm => pdm.PlanetNaturalId.ToUpper() == channelId).FirstOrDefault();
                        if (planet != null)
                        {
                            candidateDisplayName = $"{planet.PlanetName} Global Site Owners".ToUpper();
                        }
                        else
                        {
                            // If we don't have the planet data for some reason, just try the channelId
                            candidateDisplayName = $"{channelId} Global Site Owners".ToUpper();
                        }
                    }
                    else
                    {
                        // Named planet, leave it be
                        candidateDisplayName = $"{channelId} Global Site Owners".ToUpper();
                    }
                }

                model = DB.ChatModels
                    .AsNoTracking()
                    .Where(cm => cm.DisplayName.ToUpper() == candidateDisplayName)
                    .Include(cm => cm.Messages.OrderByDescending(msg => msg.MessageTimestamp).Skip(skipXMessages).Take(topXMessages))
                    .AsSplitQuery()
                    .FirstOrDefault();
                return model;
            }
        }

        private Response GetMessages(DynamicDictionary query)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                if (query.ContainsKey("channel_names"))
                {
                    string nameList = query["channel_names"];
                    List<string> names = nameList.Split(',').Select(n => n.ToUpper()).ToList();
                    List<string> apex_channels = DB.ChatModels
                        .AsNoTracking()
                        .Where(cm => names.Contains(cm.DisplayName.ToUpper()) || names.Contains(cm.NaturalId.ToUpper()))
                        .Select(cm => cm.ChannelId)
                        .ToList();
                    query["channel_ids"] = string.Join(',', apex_channels);
                }
                var messageQuery = DB.ChatMessages
                    .AsNoTracking()
                    .AsQueryable();
                if (query.ContainsKey("channel_ids"))
                {
                    string idlist = query["channel_ids"];
                    List<string> channelIds = idlist.Split(',').ToList();
                    messageQuery = messageQuery.Where(m => channelIds.Contains(m.ChatModelId));
                }
                if (query.ContainsKey("updated_since"))
                {
                    DateTime since = query["updated_since"];
                    messageQuery = messageQuery.Where(mq => mq.Timestamp > since);
                }
                messageQuery = messageQuery
                    .OrderByDescending(mq => mq.Timestamp)
                    .ThenByDescending(mq => mq.MessageTimestamp);
                if (query.ContainsKey("offset") || query.ContainsKey("skip"))
                {
                    int skip = query["offset"] != null ? query["offset"] : query["skip"];
                    messageQuery = messageQuery.Skip(skip);
                }
                if (query.ContainsKey("take") || query.ContainsKey("top"))
                {
                    int top = query["take"] != null ? query["take"] : query["top"];
                    messageQuery = messageQuery.Take(top);
                }
                var messages = messageQuery
                    .Reverse() // Output should be oldest first.
                    .ToList();
                if (messages != null && messages.Count > 0)
                {
                    return JsonConvert.SerializeObject(messages);
                }
            }
            return HttpStatusCode.NoContent;
        }

        private Response GetDisplay(string channelId, int top, int skip)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                ChatModel model = GetChatModelFromChannelIdWithMessages(DB, channelId, top, skip);
                if (model != null)
                {
                    var messages = model.Messages.Take(top).Reverse().ToList();
                    return JsonConvert.SerializeObject(messages);
                }
            }


            return HttpStatusCode.NoContent;
        }

        private Response GetPrettyDisplay(string channelId, int top, int skip)
        {
            StringBuilder sb = new StringBuilder();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                ChatModel model = GetChatModelFromChannelIdWithMessages(DB, channelId, top, skip);
                if (model != null)
                {
                    var messages = model.Messages.Take(top).Reverse().ToList();
                    foreach (var message in messages)
                    {
                        DateTime messageTime = new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(Convert.ToDouble(message.MessageTimestamp));
                        string messageTimeStr = messageTime.ToString("yyyy-MM-dd HH:mm:ss");

                        switch (message.MessageType)
                        {
                            case "CHAT":
                                sb.AppendLine($"[UTC {messageTimeStr}] {message.UserName}: {message.MessageText}");
                                break;
                            case "JOINED":
                                sb.AppendLine($"[UTC {messageTimeStr}] {message.UserName} joined.");
                                break;
                            case "LEFT":
                                sb.AppendLine($"[UTC {messageTimeStr}] {message.UserName} left.");
                                break;
                            case "DELETED":
                                sb.AppendLine($"[UTC {messageTimeStr}] {message.UserName} deleted this message.");
                                break;
                        }
                    }
                }
                else
                {
                    sb.Append("ChannelId Not Found.");
                }
            }

            Response resp = sb.ToString();
            resp.ContentType = "text/plain";
            resp.StatusCode = HttpStatusCode.OK;
            return resp;
        }

        private Response GetChatForUserInChannel(string username, string channel)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                ChatModel model = DB.ChatModels
                    .AsNoTracking()
                    .Include(cm => cm.Messages)
                    .Where(cm => cm.ChannelId.ToUpper() == channel.ToUpper() || cm.DisplayName.ToUpper() == channel.ToUpper())
                    .FirstOrDefault();
                if (model != null)
                {
                    var messages = model.Messages
                        .Where(msg => msg.UserName.ToUpper() == username.ToUpper())
                        .OrderBy(msg => msg.MessageTimestamp)
                        .TakeLast(100)
                        .ToList();
                    return JsonConvert.SerializeObject(messages);
                }
            }

            return HttpStatusCode.NoContent;
        }

        private Response GetActiveChatCount()
        {
            int count;
            using (var DB = PRUNDataContext.GetNewContext())
            {
                long ThirtyDaysAgo = DateTime.UtcNow.AddDays(-30).ToEpochMs();

                count = DB.ChatMessages
                    .AsNoTracking()
                    .Where(cm => cm.MessageTimestamp >= ThirtyDaysAgo)
                    .Select(cm => cm.UserName)
                    .Distinct()
                    .Count();
            }

            Response resp = $"{count}";
            resp.ContentType = "text/plain";
            resp.StatusCode = HttpStatusCode.OK;
            return resp;
        }

        private Response GetActiveChatCountNoLeaveJoin()
        {
            int count;
            using (var DB = PRUNDataContext.GetNewContext())
            {
                long ThirtyDaysAgo = DateTime.UtcNow.AddDays(-30).ToEpochMs();

                count = DB.ChatMessages
                    .AsNoTracking()
                    .Where(cm => cm.MessageTimestamp >= ThirtyDaysAgo)
                    .Where(cm => cm.MessageType != "JOINED" && cm.MessageType != "LEFT")
                    .Select(cm => cm.UserName)
                    .Distinct()
                    .Count();
            }

            Response resp = $"{count}";
            resp.ContentType = "text/plain";
            resp.StatusCode = HttpStatusCode.OK;
            return resp;
        }
    }
}
#endif // WITH_MODULES