﻿#if WITH_MODULES
using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class StorageModule : NancyModule
    {
        public StorageModule() : base("/storage")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostStorage();
            });

            Post("/change", _ =>
            {
                this.EnforceWriteAuth();
                return PostStorageChange();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetStorage(parameters.username);
            });

            Get("/{username}/{storage_description}", parameters =>
            {
                this.EnforceReadAuth();
                return GetStorage(parameters.username, parameters.storage_description);
            });

            Get("/planets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetStoragePlanets(parameters.username);
            });
        }

        private Response PostStorage()
        {
            using (var req = new FIORequest<JSONRepresentations.StorageStorages.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var storages = new List<Storage>();
                var data = req.JsonPayload.payload.message.payload;

                foreach (var store in data.stores)
                {
                    var storage = new Storage();
                    storage.StorageId = store.id;
                    storage.AddressableId = store.addressableId;
                    storage.Name = store.name;
                    storage.WeightLoad = store.weightLoad;
                    storage.WeightCapacity = store.weightCapacity;
                    storage.VolumeLoad = store.volumeLoad;
                    storage.VolumeCapacity = store.volumeCapacity;

                    foreach (var item in store.items)
                    {
                        var storageItem = new StorageItem();
                        if (item.type == "BLOCKED" || item.type == "SHIPMENT")
                        {
                            storageItem.Type = item.type;
                            storageItem.MaterialId = item.id;
                            storageItem.TotalWeight = item.weight;
                            storageItem.TotalVolume = item.volume;
                        }
                        else
                        {
                            storageItem.MaterialId = item.quantity.material.id;
                            storageItem.MaterialName = item.quantity.material.name;
                            storageItem.MaterialTicker = item.quantity.material.ticker;
                            storageItem.MaterialCategory = item.quantity.material.category;
                            storageItem.MaterialWeight = item.quantity.material.weight;
                            storageItem.MaterialVolume = item.quantity.material.volume;
                            storageItem.MaterialAmount = item.quantity.amount;
                            if (item.quantity.value != null)
                            {
                                storageItem.MaterialValue = item.quantity.value.amount;
                                storageItem.MaterialValueCurrency = item.quantity.value.currency;
                            }

                            storageItem.Type = item.type;
                            storageItem.TotalWeight = item.weight;
                            storageItem.TotalVolume = item.volume;
                        }
                        storageItem.StorageItemId = $"{storage.StorageId}-{storageItem.MaterialId}";
                        storageItem.StorageId = storage.StorageId;

                        storage.StorageItems.Add(storageItem);
                    }

                    storage.FixedStore = store.@fixed;
                    storage.Type = store.type;

                    storage.UserNameSubmitted = req.UserName;
                    storage.Timestamp = req.Now;

                    storage.Validate();
                    storages.Add(storage);
                }

                req.DB.Storages.RemoveRange(req.DB.Storages.Where(s => s.UserNameSubmitted.ToUpper() == req.UserName));
                req.PostRemoveRangeIgnoreConcurrencyFailures();

                req.DB.Storages.UpsertRange(storages)
                    .On(s => new { s.StorageId })
                    .Run();
                
                var storageItems = storages.SelectMany(s => s.StorageItems).ToList();
                req.DB.StorageItems.UpsertRange(storageItems)
                    .On(si => new { si.StorageItemId })
                    .Run();

                return HttpStatusCode.OK;
            }
        }

        private Response PostStorageChange()
        {
            using (var req = new FIORequest<JSONRepresentations.StorageChange.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;

                var storages = new List<Storage>();

                foreach (var store in rootObj.payload.stores)
                {
                    var storage = new Storage();
                    storage.StorageId = store.id;
                    storage.AddressableId = store.addressableId;
                    storage.Name = store.name;
                    storage.WeightLoad = store.weightLoad;
                    storage.WeightCapacity = store.weightCapacity;
                    storage.VolumeLoad = store.volumeLoad;
                    storage.VolumeCapacity = store.volumeCapacity;

                    foreach (var item in store.items)
                    {
                        var storageItem = new StorageItem();
                        if (item.type == "BLOCKED" || item.type == "SHIPMENT")
                        {
                            storageItem.Type = item.type;
                            storageItem.MaterialId = item.id;
                            storageItem.TotalWeight = item.weight;
                            storageItem.TotalVolume = item.volume;
                        }
                        else
                        {
                            storageItem.MaterialId = item.quantity.material.id;
                            storageItem.MaterialName = item.quantity.material.name;
                            storageItem.MaterialTicker = item.quantity.material.ticker;
                            storageItem.MaterialCategory = item.quantity.material.category;
                            storageItem.MaterialWeight = item.quantity.material.weight;
                            storageItem.MaterialVolume = item.quantity.material.volume;
                            storageItem.MaterialAmount = item.quantity.amount;
                            if (item.quantity.value != null)
                            {
                                storageItem.MaterialValue = item.quantity.value.amount;
                                storageItem.MaterialValueCurrency = item.quantity.value.currency;
                            }

                            storageItem.Type = item.type;
                            storageItem.TotalWeight = item.weight;
                            storageItem.TotalVolume = item.volume;
                        }
                        storageItem.StorageItemId = $"{storage.StorageId}-{storageItem.MaterialId}";
                        storageItem.StorageId = storage.StorageId;

                        storage.StorageItems.Add(storageItem);
                    }

                    storage.FixedStore = store._fixed;
                    storage.Type = store.type;

                    storage.UserNameSubmitted = req.UserName;
                    storage.Timestamp = req.Now;

                    storage.Validate();
                    storages.Add(storage);
                }

                req.DB.Storages.UpsertRange(storages)
                    .On(s => new { s.StorageId })
                    .Run();

                var storageItems = storages.SelectMany(s => s.StorageItems);
                req.DB.StorageItems.UpsertRange(storageItems)
                    .On(si => new { si.StorageItemId })
                    .Run();

                return HttpStatusCode.OK;
            }
        }

        private Response GetStorage(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var storages = DB.Storages
                        .Include(s => s.StorageItems)
                        .AsNoTracking()
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(storages);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetStorage(string UserName, string StorageDesc)
        {
            UserName = UserName.ToUpper();
            StorageDesc = StorageDesc.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var site = DB.Sites
                        .AsNoTracking()
                        .Include(s => s.Buildings)
                            .ThenInclude(b => b.ReclaimableMaterials)
                        .Include(s => s.Buildings)
                            .ThenInclude(b => b.RepairMaterials)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName && (s.PlanetId.ToUpper() == StorageDesc || s.PlanetIdentifier.ToUpper() == StorageDesc || s.PlanetName.ToUpper() == StorageDesc))
                        .AsSplitQuery()
                        .FirstOrDefault();
                    if (site != null)
                    {
                        var model = DB.Storages
                            .AsNoTracking()
                            .Include(s => s.StorageItems)
                            .Where(s => s.UserNameSubmitted.ToUpper() == UserName && s.AddressableId == site.SiteId && s.StorageId != null)
                            .FirstOrDefault();
                        return JsonConvert.SerializeObject(model);
                    }

                    var storage = DB.Storages
                        .AsNoTracking()
                        .Include(s => s.StorageItems)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName && (s.StorageId.ToUpper() == StorageDesc))
                        .FirstOrDefault();
                    if (storage != null)
                    {
                        return JsonConvert.SerializeObject(storage);
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetStoragePlanets(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var storagePlanets = DB.Sites
                        .AsNoTracking()
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .Select(s => s.PlanetIdentifier)
                        .ToList();

                    return JsonConvert.SerializeObject(storagePlanets);
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
#endif // WITH_MODULES