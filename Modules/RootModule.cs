﻿#if WITH_MODULES
using Nancy;

namespace FIORest
{
    public class RootModule : NancyModule
    {
        public RootModule()
        {
            Get("/", _ =>
            {
                Response resp = new Response();
                resp.StatusCode = HttpStatusCode.MovedPermanently;
                resp.Headers.Add("Location", "https://doc.fnar.net");
                return resp;
            });
        }
    }
}
#endif // WITH_MODULES