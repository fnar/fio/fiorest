﻿#if WITH_MODULES
using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using System.Linq;

using Microsoft.EntityFrameworkCore;
using Nancy;
using Newtonsoft.Json;

namespace FIORest.Modules
{
    public class CreatePayload
    {
        public string UserName;
        public string Password;
        public bool IsAdmin;
    }

    public class DisablePayload
    {
        public string UserName;
        public string Reason;
    }

    public class AdminModule : NancyModule
    {
        public AdminModule() : base("/admin")
        {
            this.EnforceAuthAdmin();

            Get("/", _ =>
            {
                return HttpStatusCode.OK;
            });

            Get("/{username}", parameters =>
            {
                return UserNameExists(parameters.username);
            });

            Get("/allusers", _ =>
            {
                return GetAllUsers();
            });

            Get("/usercount", _ =>
            {
                return GetUserCount();
            });

            Get("/requestdata", _ =>
            {
                return GetRequestData();
            });

            Get("/endpointrequestdata", _ =>
            {
                return GetEndpointRequestData();
            });

            Get("/timingdata", _ =>
            {
                return GetTimingData();
            });

            Post("/create", _ =>
            {
                return Create();
            });

            Post("/disable", _ =>
            {
                return Disable();
            });

            Post("/clearcxdata", _ =>
            {
                return PostClearCXData();
            });

            Post("/clearcxpcdata", _ =>
            {
                return PostClearCXPCData();
            });

            Post("/clearbuildingdata", _ =>
            {
                return PostClearBuildingData();
            });

            Post("/clearmatdata", _ =>
            {
                return PostClearMatData();
            });

            Post("/clearjumpcache", _ =>
            {
                return PostClearJumpCache();
            });

            Post("/forceupdatesystemid", _ =>
            {
                return PostForceUpdateSystemId();
            });

            Post("/resetuserdata/{username}", parameters =>
            {
                return PostResetUserData(parameters.username);
            });

            Post("/resetuserdatatable", parameters =>
            {
                return PostPurgeUserDataTable();
            });

            Post("/recalculateplanettiers", _ =>
            {
                return PostRecalculatePlanetTiers();
            });

            Post("/cleardegradationdata", _ =>
            {
                return PostClearDegradationData();
            });

            Post("/startdebugtracking/{username}", parameters =>
            {
                return PostStartDebugTracking(parameters.username);
            });

            Post("/stopdebugtracking/{username}", parameters =>
            {
                return PostStopDebugTracking(parameters.username);
            });
        }

        private Response UserNameExists(string UserName)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var authModel = DB.AuthenticationModels
                    .AsNoTracking()
                    .Where(a => a.UserName.ToUpper() == UserName.ToUpper())
                    .FirstOrDefault();
                if (authModel != null)
                {
                    return HttpStatusCode.OK;
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetAllUsers()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var names = DB.ChatMessages
                    .AsNoTracking()
                    .Select(cm => cm.UserName)
                    .Distinct()
                    .OrderBy(cm => cm)
                    .ToList();
                return JsonConvert.SerializeObject(names);
            }
        }

        private Response GetUserCount()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var count = DB.ChatMessages
                    .AsNoTracking()
                    .Select(cm => cm.UserName)
                    .Distinct()
                    .Count();
                return $"{count}";
            }
        }

        private Response GetRequestData()
        {
            return JsonConvert.SerializeObject(RequestTracker.GetRequestData().OrderByDescending(rd => rd.RequestCount).ToList());
        }

        private Response GetEndpointRequestData()
        {
            return JsonConvert.SerializeObject(RequestTracker.GetEndpointRequestData().OrderByDescending(rd => rd.TotalBytesUploaded).ToList());
        } 

        private Response GetTimingData()
        {
            return JsonConvert.SerializeObject(TimingTracker.GetTimingData().OrderByDescending(td => td.AvgMS).ToList());
        }

        private Response Create()
        {
            using (var req = new FIORequest<CreatePayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var createPayload = req.JsonPayload;
                var authModel = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == createPayload.UserName.ToUpper()).FirstOrDefault();
                bool bShouldAdd = (authModel == null);
                if (bShouldAdd)
                {
                    authModel = new AuthenticationModel();
                }

                authModel.AccountEnabled = true;
                authModel.UserName = createPayload.UserName;
                authModel.PasswordHash = SecurePasswordHasher.Hash(createPayload.Password);
                authModel.IsAdministrator = createPayload.IsAdmin;

                if (bShouldAdd)
                {
                    req.DB.AuthenticationModels.Add(authModel);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        public Response Disable()
        {
            using (var req = new FIORequest<DisablePayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var disablePayload = req.JsonPayload;
                var authModel = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == disablePayload.UserName.ToUpper()).FirstOrDefault();
                if (authModel != null)
                {
                    authModel.AccountEnabled = false;
                    authModel.DisabledReason = disablePayload.Reason;
                    req.DB.SaveChanges();
                    return HttpStatusCode.OK;
                }

                return HttpStatusCode.BadRequest;
            }
        }

        private Response PostClearCXData()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                var all = from c in DB.CXDataModels select c;
                DB.CXDataModels.RemoveRange(all);

                var allStations = from s in DB.Stations select s;
                DB.Stations.RemoveRange(allStations);

                var allComexExchanges = from c in DB.ComexExchanges select c;
                DB.ComexExchanges.RemoveRange(allComexExchanges);

                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostClearCXPCData()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                var all = from c in DB.CXPCData select c;
                DB.CXPCData.RemoveRange(all);
                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostClearBuildingData()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                var all = from c in DB.Buildings select c;
                DB.Buildings.RemoveRange(all);
                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostClearMatData()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                var all = from c in DB.Materials select c;
                DB.Materials.RemoveRange(all);
                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostClearJumpCache()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                var all = from c in DB.JumpCache select c;
                DB.JumpCache.RemoveRange(all);
                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        // Fixup
        private Response PostForceUpdateSystemId()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                var AllPlanetsData = DB.PlanetDataModels.ToList();
                for (int i = 0; i < AllPlanetsData.Count; ++i)
                {
                    var systemIdFromPlanetNaturalId = AllPlanetsData[i].PlanetNaturalId.Substring(0, (AllPlanetsData[i].PlanetNaturalId.Length - 1));
                    var system = DB.Systems.Where(ssm => ssm.NaturalId == systemIdFromPlanetNaturalId).FirstOrDefault();
                    if (system == null)
                    {
                        return HttpStatusCode.BadRequest;
                    }

                    AllPlanetsData[i].SystemId = system.SystemId;
                }

                DB.SaveChanges();
                transaction.Commit();
            }

            return HttpStatusCode.OK;
        }

        private Response PostResetUserData(string UserName)
        {
            UserName = UserName.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                DB.Companies.RemoveRange(DB.Companies.Where(c => c.UserNameSubmitted.ToUpper() == UserName));
                DB.ProductionLines.RemoveRange(DB.ProductionLines.Where(p => p.UserNameSubmitted.ToUpper() == UserName));
                DB.Ships.RemoveRange(DB.Ships.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.Sites.RemoveRange(DB.Sites.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.Storages.RemoveRange(DB.Storages.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.Workforces.RemoveRange(DB.Workforces.Where(s => s.UserNameSubmitted.ToUpper() == UserName));

                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostPurgeUserDataTable()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                DB.UserData.RemoveRange(DB.UserData);

                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostRecalculatePlanetTiers()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                foreach (var planet in DB.PlanetDataModels)
                {
                    planet.UpdatePlanetTier();
                }

                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostClearDegradationData()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                DB.BuildingDegradations.RemoveRange(DB.BuildingDegradations);
                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }

        private Response PostStartDebugTracking(string username)
        {
            UserDataTracking.AddUser(username);
            return HttpStatusCode.OK;
        }

        private Response PostStopDebugTracking(string username)
        {
            UserDataTracking.RemoveUser(username);
            return HttpStatusCode.OK;
        }
    }
}
#endif // WITH_MODULES
