﻿#if WITH_MODULES
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

using Nancy;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

using Newtonsoft.Json;

namespace FIORest.Modules
{
    public class GlobalModule : NancyModule
    {
        public GlobalModule() : base("/global")
        {
            this.Cacheable();

            Post("/comexexchanges", _ =>
            {
                this.EnforceAuthAdmin();
                return PostComexExchanges();
            });

            Post("/countries", _ =>
            {
                this.EnforceAuthAdmin();
                return PostCountries();
            });

            Post("/simulationdata", _ =>
            {
                this.EnforceAuthAdmin();
                return PostSimulationData();
            });

            Post("/workforceneeds", _ =>
            {
                this.EnforceAuthAdmin();
                return PostWorkforceNeeds();
            });

            Get("/comexexchanges", _ =>
            {
                return GetComexExchanges();
            });

            Get("/countries", _ =>
            {
                return GetCountries();
            });

            Get("/simulationdata", _ =>
            {
                return GetSimulationData();
            });

            Get("/workforceneeds", _ =>
            {
                return GetWorkforceNeeds();
            });
        }

        private Response PostComexExchanges()
        {
            using (var req = new FIORequest<JSONRepresentations.ComexExchangeList.Rootobject>(Request, FIOAPIEndpoint:"/global/comexexchanges"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var allExchanges = new List<ComexExchange>();

                var exchanges = req.JsonPayload.payload.message.payload.body;
                foreach( var exchange in exchanges)
                {
                    var exch = new ComexExchange();

                    exch.ComexExchangeId = exchange.id;
                    exch.ExchangeName = exchange.name;
                    exch.ExchangeCode = exchange.code;
                    if (exchange._operator != null)
                    {
                        exch.ExchangeOperatorId = exchange._operator.id;
                        exch.ExchangeOperatorName = exchange._operator.name;
                        exch.ExchangeOperatorCode = exchange._operator.code;
                    }

                    if (exchange.currency != null)
                    {
                        exch.CurrencyNumericCode = exchange.currency.numericCode;
                        exch.CurrencyCode = exchange.currency.code;
                        exch.CurrencyName = exchange.currency.name;
                        exch.CurrencyDecimals = exchange.currency.decimals;
                    }

                    if (exchange.address != null && exchange.address.lines.Length > 0)
                    {
                        var lastLine = exchange.address.lines[exchange.address.lines.Length - 1];
                        exch.LocationId = lastLine.entity.id;
                        exch.LocationName = lastLine.entity.name;
                        exch.LocationNaturalId = lastLine.entity.naturalId;
                    }

                    exch.Validate();
                    allExchanges.Add(exch);
                }

                req.DB.ComexExchanges.UpsertRange(allExchanges)
                    .On(ex => new { ex.ComexExchangeId })
                    .Run();

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostCountries()
        {
            using (var req = new FIORequest<JSONRepresentations.CountryRegistryCountries.Rootobject>(Request, FIOAPIEndpoint:"/global/countries"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var allCountries = new List<CountryRegistryCountry>();

                var countries = req.JsonPayload.payload.message.payload.countries;
                foreach (var country in countries)
                {
                    var newCountry = new CountryRegistryCountry();

                    newCountry.CountryRegistryCountryId = country.id;
                    newCountry.CountryCode = country.code;
                    newCountry.CountryName = country.name;

                    newCountry.CurrencyNumericCode = country.currency.numericCode;
                    newCountry.CurrencyCode = country.currency.code;
                    newCountry.CurrencyName = country.currency.name;
                    newCountry.CurrencyDecimals = country.currency.decimals;

                    newCountry.Validate();
                    allCountries.Add(newCountry);
                }

                req.DB.CountryRegistryCountries.UpsertRange(allCountries)
                    .On(c => new { c.CountryRegistryCountryId })
                    .Run();

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostSimulationData()
        {
            using (var req = new FIORequest<JSONRepresentations.SimulationData.Rootobject>(Request, FIOAPIEndpoint:"/global/simulationdata"))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var simulationData = req.JsonPayload.payload.message.payload;

                var existingSimulationData = req.DB.SimulationData.FirstOrDefault();
                bool bAdd = (existingSimulationData == null);
                if (bAdd)
                {
                    existingSimulationData = new SimulationData();
                }

                existingSimulationData.SimulationInterval = simulationData.simulationInterval;
                existingSimulationData.FlightSTLFactor = simulationData.flightSTLFactor;
                existingSimulationData.FlightFTLFactor = simulationData.flightFTLFactor;
                existingSimulationData.PlanetaryMotionFactor = simulationData.planetaryMotionFactor;
                existingSimulationData.ParsecLength = simulationData.parsecLength;


                if (bAdd)
                {
                    req.DB.SimulationData.Add(existingSimulationData);
                }

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostWorkforceNeeds()
        {
            using (var req = new FIORequest<JSONRepresentations.WorkforceWorkforces.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var workforces = req.JsonPayload.payload.message.payload.workforces;
                foreach (var workforce in workforces)
                {
                    var workforcePer100 = req.DB.WorkforcePerOneHundreds.Where(wpoh => wpoh.WorkforceType.ToUpper() == workforce.level.ToUpper()).FirstOrDefault();
                    bool bAddWorkforcePer100 = (workforcePer100 == null);
                    if (bAddWorkforcePer100)
                    {
                        workforcePer100 = new WorkforcePerOneHundred();
                    }

                    workforcePer100.WorkforceType = workforce.level;

                    foreach (var need in workforce.needs)
                    {
                        var workforcePer100Need = workforcePer100.Needs.Where(n => n.MaterialTicker.ToUpper() == need.material.ticker.ToUpper()).FirstOrDefault();
                        bool bAddWorkforcePer100Need = (workforcePer100Need == null);
                        if (bAddWorkforcePer100Need)
                        {
                            workforcePer100Need = new WorkforcePerOneHundreedNeed();
                        }

                        workforcePer100Need.MaterialId = need.material.id;
                        workforcePer100Need.MaterialName = need.material.name;
                        workforcePer100Need.MaterialTicker = need.material.ticker;
                        workforcePer100Need.MaterialCategory = need.material.category;
                        workforcePer100Need.Amount = need.unitsPer100;

                        if (bAddWorkforcePer100Need)
                        {
                            workforcePer100.Needs.Add(workforcePer100Need);
                        }
                    }

                    if (bAddWorkforcePer100)
                    {
                        req.DB.WorkforcePerOneHundreds.Add(workforcePer100);
                    }

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response GetComexExchanges()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var comexExchanges = DB.ComexExchanges
                    .AsNoTracking()
                    .ToList();
                return JsonConvert.SerializeObject(comexExchanges);
            }
        }

        private Response GetCountries()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var countries = DB.CountryRegistryCountries
                    .AsNoTracking()
                    .ToList();
                return JsonConvert.SerializeObject(countries);
            }
        }

        private Response GetSimulationData()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var simData = DB.SimulationData
                    .AsNoTracking()
                    .FirstOrDefault();
                if (simData != null)
                {
                    return JsonConvert.SerializeObject(simData);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetWorkforceNeeds()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var workforceNeeds = DB.WorkforcePerOneHundreds
                    .AsNoTracking()
                    .Include(wpoh => wpoh.Needs)
                    .AsSplitQuery()
                    .ToList();
                return JsonConvert.SerializeObject(workforceNeeds);
            }
        }
    }
}
#endif // WITH_MODULES