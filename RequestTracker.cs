﻿#define REQUEST_TRACKER_ENABLED

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;

using FIORest.Authentication;
using Nancy;
using Nancy.LeakyBucket.Identifiers;

namespace FIORest
{
    public class RequestData
    {
        public DateTime LastTimeStamp { get; set; }
        public string IP { get; set; }
        public string AuthToken { get; set; }
        public string UserName { get; set; }
        public int RequestCount { get; set; }
        public int RequestRateLimitCount { get; set; }
        public long BytesUploaded { get; set; }
        public Dictionary<string, int> PathToCount { get; set; } = new Dictionary<string, int>();
    }

    public class EndpointRequestData
    {
        public string Path { get; set; }
        public long MinBytesUploaded { get; set; }
        public long MaxBytesUploaded { get; set; }
        public long RequestCount { get; set; }
        public long TotalBytesUploaded { get; set; }
    }

    public static class RequestTracker
    {
        private const string XForwardedForHeaderKey = "X-Forwarded-For";
        private static object _IPToRequestDataLock = new object();
        private static Dictionary<string, RequestData> IPToRequestData = new Dictionary<string, RequestData>();

        private static object _PathToEndpointRequestDataLock = new object();
        private static Dictionary<string, EndpointRequestData> PathToEndpointRequestData = new Dictionary<string, EndpointRequestData>();

        public static void Store(Request req)
        {
#if REQUEST_TRACKER_ENABLED
            string IP;
            if (req.Headers.Keys.Contains(XForwardedForHeaderKey))
            {
                IP = String.Join(";", req.Headers[XForwardedForHeaderKey]);
            }
            else
            {
                IP = req.UserHostAddress;
            }

            string Identifier = IP + "-";

            string AuthToken = null;
            if (req.Headers.Keys.Contains("Authorization"))
            {
                AuthToken = req.Headers["Authorization"].ToList()[0];
                Identifier += AuthToken;
            }

            string UserName = Auth.GetUserName(req);
            string Path = req.Path;
            long Bytes = req.Body.Length;

            bool bExists = false;

            lock(_IPToRequestDataLock)
            {
                bExists = IPToRequestData.ContainsKey(Identifier);
                if (bExists)
                {
                    IPToRequestData[Identifier].LastTimeStamp = DateTime.UtcNow;
                    IPToRequestData[Identifier].AuthToken = AuthToken;
                    IPToRequestData[Identifier].UserName = UserName;
                    IPToRequestData[Identifier].RequestCount++;
                    IPToRequestData[Identifier].BytesUploaded += Bytes;
                    if (!IPToRequestData[Identifier].PathToCount.TryAdd(Path, 1))
                    {
                        IPToRequestData[Identifier].PathToCount[Path]++;
                    }
                }
                else
                {
                    RequestData data = new RequestData();
                    data.LastTimeStamp = DateTime.UtcNow;
                    data.IP = IP;
                    data.AuthToken = AuthToken;
                    data.UserName = UserName;
                    data.RequestCount = 1;
                    data.BytesUploaded = Bytes;
                    data.PathToCount.TryAdd(Path, 1);
                    if (!IPToRequestData.TryAdd(Identifier, data))
                    {
                        IPToRequestData[Identifier].LastTimeStamp = DateTime.UtcNow;
                        IPToRequestData[Identifier].RequestCount++;
                        IPToRequestData[Identifier].BytesUploaded += Bytes;
                        if (!IPToRequestData[Identifier].PathToCount.TryAdd(Path, 1))
                        {
                            IPToRequestData[Identifier].PathToCount[Path]++;
                        }
                    }
                }
            }

            lock(_PathToEndpointRequestDataLock)
            {
                bExists = PathToEndpointRequestData.ContainsKey(Path);
                if (bExists)
                {
                    PathToEndpointRequestData[Path].MinBytesUploaded = Math.Min(PathToEndpointRequestData[Path].MinBytesUploaded, Bytes);
                    PathToEndpointRequestData[Path].MaxBytesUploaded = Math.Max(PathToEndpointRequestData[Path].MaxBytesUploaded, Bytes);
                    PathToEndpointRequestData[Path].RequestCount++;
                    PathToEndpointRequestData[Path].TotalBytesUploaded += Bytes;
                }
                else
                {
                    var data = new EndpointRequestData();
                    data.Path = Path;
                    data.MinBytesUploaded = Bytes;
                    data.MaxBytesUploaded = Bytes;
                    data.RequestCount = 1;
                    data.TotalBytesUploaded = Bytes;
                    PathToEndpointRequestData.Add(Path, data);
                }
            }

#endif
        }

        public static List<RequestData> GetRequestData()
        {
            lock(_IPToRequestDataLock)
            {
                return IPToRequestData.Values.ToList();
            }
        }

        public static List<EndpointRequestData> GetEndpointRequestData()
        {
            lock(_PathToEndpointRequestDataLock)
            {
                return PathToEndpointRequestData.Values.ToList();
            }
        }

        public static void StoreRateLimitHit(IClientIdentifier identifier)
        {
#if REQUEST_TRACKER_ENABLED
            XForwardedForClientIdentifier xident = identifier as XForwardedForClientIdentifier;
            if (xident != null && IPToRequestData.ContainsKey(xident.Identifier))
            {
                IPToRequestData[xident.Identifier].RequestRateLimitCount++;
            }
#endif
        }
    }
}
