#!/bin/sh

dotnet ef migrations add $1 --context=SqliteDataContext --output-dir=Migrations/SqliteMigrations
dotnet ef migrations add $1 --context=PostgresDataContext --output-dir=Migrations/PostgresMigrations
#dotnet ef migrations add $1 --context=SqlServerDataContext --output-dir=Migrations/SqlServer
