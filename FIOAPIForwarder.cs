using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

﻿using FIORest.Database.Models;

namespace FIORest
{
	public static class FIOAPIForwarder
	{
		// @TODO: Extract to json
		private const string APIKey = "15be3777-8808-4049-8674-85b0d1dcf615";
		private const string RootUrl = "https://api.fnar.net";
		private static readonly HttpClient HttpClient;

		static FIOAPIForwarder()
		{
			HttpClientHandler handler = new HttpClientHandler()
			{
				AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
			};
			HttpClient = new HttpClient(handler);
			HttpClient.Timeout = TimeSpan.FromSeconds(300.0);
		}

		public static async Task Forward(string Endpoint, string Payload)
		{
			if (string.IsNullOrWhiteSpace(Endpoint))
				return;

			if (!Endpoint.StartsWith("/"))
			{
				Endpoint = "/" + Endpoint;
			}

			try
			{
				using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Put, RootUrl + Endpoint))
				{
					httpRequestMessage.Headers.Add("Authorization", $"FIOAPIKey {APIKey}");
					httpRequestMessage.Content = new StringContent(Payload, Encoding.UTF8, "application/json");
					using var resp = await HttpClient.SendAsync(httpRequestMessage);
				}
			}
			catch
			{
				// Soft-fail
			}
		}
	}
}
