﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class CXOSTradeOrder
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CXOSTradeOrderId { get; set; }

        [StringLength(64)]
        public string ExchangeName { get; set; }
        [StringLength(8)]
        public string ExchangeCode { get; set; }

        [StringLength(32)]
        public string BrokerId { get; set; }
        [StringLength(16)]
        public string OrderType { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }

        public int Amount { get; set; }
        public int InitialAmount { get; set; }

        public double Limit { get; set; }
        [StringLength(8)]
        public string LimitCurrency { get; set; }

        [StringLength(16)]
        public string Status { get; set; }
        public long CreatedEpochMs { get; set; }

        public virtual List<CXOSTrade> Trades { get; set; } = new List<CXOSTrade>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(CXOSTradeOrderId) && CXOSTradeOrderId.Length == 32 &&
                !String.IsNullOrEmpty(ExchangeName) && ExchangeName.Length <= 64 &&
                !String.IsNullOrEmpty(ExchangeCode) && ExchangeCode.Length <= 8 &&
                !String.IsNullOrEmpty(BrokerId) && BrokerId.Length == 32 &&
                !String.IsNullOrEmpty(OrderType) && OrderType.Length <= 16 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(LimitCurrency) && LimitCurrency.Length <= 8 &&
                !String.IsNullOrEmpty(Status) && Status.Length <= 16 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            Trades.ForEach(t => t.Validate());
        }
    }

    public class CXOSTrade
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CXOSTradeId { get; set; }

        public int Amount { get; set; }

        public double Price { get; set; }

        [StringLength(8)]
        public string PriceCurrency { get; set; }

        public long TradeTimeEpochMs { get; set; }

        [StringLength(32)]
        public string PartnerId { get; set; }
        [StringLength(64)]
        public string PartnerName { get; set; }
        [StringLength(8)]
        public string PartnerCode { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string CXOSTradeOrderId { get; set; }
        [JsonIgnore]
        public virtual CXOSTradeOrder CXOSTradeOrder { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(CXOSTradeId) && CXOSTradeId.Length == 32 &&
                Amount > 0 &&
                Price > 0.0 &&
                !String.IsNullOrEmpty(PriceCurrency) && PriceCurrency.Length <= 8 &&
                !String.IsNullOrEmpty(PartnerId) && PartnerId.Length == 32 &&
                (PartnerName == null || PartnerName.Length <= 64) &&
                (PartnerCode == null || PartnerCode.Length <= 8) &&
                !String.IsNullOrEmpty(CXOSTradeOrderId) && CXOSTradeOrderId.Length == 32
                );
        }
    }
}
