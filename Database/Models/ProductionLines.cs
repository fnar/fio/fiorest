﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(SiteId))]
    [Index(nameof(PlanetId))]
    [Index(nameof(PlanetNaturalId))]
    [Index(nameof(PlanetName))]
    public class ProductionLine
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ProductionLineId { get; set; }

        [StringLength(32)]
        public string SiteId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }

        [StringLength(8)]
        public string PlanetNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string Type { get; set; }

        public int Capacity { get; set; }

        public double Efficiency { get; set; }
        public double Condition { get; set; }

        public virtual List<ProductionLineOrder> Orders { get; set; } = new List<ProductionLineOrder>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ProductionLineId) && ProductionLineId.Length == 32 &&
                !String.IsNullOrEmpty(SiteId) && SiteId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetId) && PlanetId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetNaturalId) && PlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(PlanetName) && PlanetName.Length <= 32 &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 32 &&
                Capacity > 0 &&
                Efficiency >= 0.0 &&
                Condition > 0.0 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            Orders.ForEach(o => o.Validate());
        }
    }

    public class ProductionLineOrder
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ProductionLineOrderId { get; set; }

        public virtual List<ProductionLineInput> Inputs { get; set; } = new List<ProductionLineInput>();
        public virtual List<ProductionLineOutput> Outputs { get; set; } = new List<ProductionLineOutput>();

        public long? CreatedEpochMs { get; set; }
        public long? StartedEpochMs { get; set; }
        public long? CompletionEpochMs { get; set; }
        public long? DurationMs { get; set; }
        public long? LastUpdatedEpochMs { get; set; }
        public double? CompletedPercentage { get; set; }

        public bool IsHalted { get; set; }
        public bool Recurring { get; set; }

        [StringLength(100)]
        public string StandardRecipeName { get; set; }

        public double ProductionFee { get; set; }

        [StringLength(8)]
        public string ProductionFeeCurrency { get; set; }

        [StringLength(32)]
        public string ProductionFeeCollectorId { get; set; }

        [StringLength(64)]
        public string ProductionFeeCollectorName { get; set; }

        [StringLength(8)]
        public string ProductionFeeCollectorCode { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string ProductionLineId { get; set; }

        [JsonIgnore]
        public virtual ProductionLine ProductionLine { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ProductionLineOrderId) && ProductionLineOrderId.Length == 32 &&
                (CreatedEpochMs == null || CreatedEpochMs > 0) &&
                (DurationMs == null || DurationMs > 0) &&
                (CompletedPercentage == null || CompletedPercentage >= 0.0) &&
                (StandardRecipeName != null && StandardRecipeName.Length <= 100) &&
                (ProductionFeeCurrency == null || ProductionFeeCurrency.Length <= 8) &&
                (ProductionFeeCollectorId == null || ProductionFeeCollectorId.Length == 32) &&
                (ProductionFeeCollectorName == null || ProductionFeeCollectorName.Length <= 64) &&
                (ProductionFeeCollectorCode == null || ProductionFeeCollectorCode.Length <= 8) &&
                !String.IsNullOrEmpty(ProductionLineId) && ProductionLineId.Length == 32
                );

            Inputs.ForEach(i => i.Validate());
            Outputs.ForEach(o => o.Validate());
        }

        private static List<string> Exclusions = new()
        {
            "RIG", "EXT", "COL"
        };

        public string GetStandardRecipeName(PRUNDataContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (ProductionLine == null) throw new ArgumentNullException(nameof(ProductionLine));

            var BuildingTicker = context.Buildings
                .Where(b => b.Name == ProductionLine.Type)
                .Select(b => b.Ticker)
                .FirstOrDefault();

            if (BuildingTicker != null)
            {
                List<string> inputs = new();
                List<string> outputs = new();
                if (!Exclusions.Contains(BuildingTicker))
                {
                    inputs = Inputs.OrderBy(i => i.MaterialTicker).Select(i => $"{i.MaterialAmount}x{i.MaterialTicker}").ToList();
                    outputs = Outputs.OrderBy(o => o.MaterialTicker).Select(o => $"{o.MaterialAmount}x{o.MaterialTicker}").ToList();
                }

                return $"{BuildingTicker}:{String.Join("-", inputs)}=>{String.Join("-", outputs)}";
            }

            return null;
        }
    }

    public class ProductionLineInput
    {
        [Key]
        [StringLength(41)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ProductionLineInputId { get; set; } // ProductionLineOrderId-MaterialTicker

        [StringLength(64)]
        public string MaterialName { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }

        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public string ProductionLineOrderId { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public virtual ProductionLineOrder ProductionLineOrder { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ProductionLineInputId) && ProductionLineInputId.Length <= 41 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(ProductionLineOrderId) && ProductionLineOrderId.Length == 32
                );
        }
    }

    public class ProductionLineOutput
    {
        [Key]
        [StringLength(41)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ProductionLineOutputId { get; set; } // ProductionLineOrderId-MaterialTicker

        [StringLength(64)]
        public string MaterialName { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }

        public int MaterialAmount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string ProductionLineOrderId { get; set; }

        [JsonIgnore]
        public virtual ProductionLineOrder ProductionLineOrder { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ProductionLineOutputId) && ProductionLineOutputId.Length <= 41 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(ProductionLineOrderId) && ProductionLineOrderId.Length == 32
                );
        }
    }
}
