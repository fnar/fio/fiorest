﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

using Microsoft.EntityFrameworkCore;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
	[Index(nameof(BuildingTicker), nameof(DaysSinceLastRepair), nameof(HasBeenRepaired), IsUnique = true)]
	public class BuildingDegradation
	{
		public BuildingDegradation()
		{

		}													 

		public BuildingDegradation(SiteBuilding building)
		{
			bool bRepaired = (building.BuildingLastRepair != null);
			long buildingAgeMs = Utils.GetCurrentEpochMs() - (bRepaired ? (long)building.BuildingLastRepair : building.BuildingCreated);
			TimeSpan buildingAgeTimeSpan = TimeSpan.FromMilliseconds(buildingAgeMs);
			double buildingAgeDays = buildingAgeTimeSpan.TotalDays.RoundToDecimalPlaces(2);

			BuildingTicker = building.BuildingTicker;
			BuildingCreated = building.BuildingCreated;
			HasBeenRepaired = bRepaired;
			Condition = building.Condition;
			DaysSinceLastRepair = buildingAgeDays;

			if (building.ReclaimableMaterials != null)
			{
				foreach (var reclaimableMaterial in building.ReclaimableMaterials)
				{
					var bdrmm = new BuildingDegradationReclaimableMaterial();
					bdrmm.MaterialTicker = reclaimableMaterial.MaterialTicker;
					bdrmm.MaterialCount = reclaimableMaterial.MaterialAmount;

					ReclaimableMaterials.Add(bdrmm);
				}
			}

			if (building.RepairMaterials != null)
			{
				foreach (var repairMaterial in building.RepairMaterials)
				{
					var bdrmm = new BuildingDegradationRepairMaterial();
					bdrmm.MaterialTicker = repairMaterial.MaterialTicker;
					bdrmm.MaterialCount = repairMaterial.MaterialAmount;

					RepairMaterials.Add(bdrmm);
				}
			}

			Validate();
		}

		[Key]
		[JsonIgnore]
		public int BuildingDegradationId { get; set; }

		[StringLength(8)]
		public string BuildingTicker { get; set; }
		public long BuildingCreated { get; set; }
		public bool HasBeenRepaired { get; set; }

		public double Condition { get; set; }
		public double DaysSinceLastRepair { get; set; }

		public virtual List<BuildingDegradationReclaimableMaterial> ReclaimableMaterials { get; set; } = new List<BuildingDegradationReclaimableMaterial>();
		public virtual List<BuildingDegradationRepairMaterial> RepairMaterials { get; set; } = new List<BuildingDegradationRepairMaterial>();

		public void Validate()
		{
			Debug.Assert(!String.IsNullOrEmpty(BuildingTicker) && BuildingTicker.Length <= 8);

			ReclaimableMaterials.ForEach(rm => rm.Validate());
			RepairMaterials.ForEach(rm => rm.Validate());
		}
	}

	[Index(nameof(BuildingDegradationId), nameof(MaterialTicker), IsUnique = true)]
	public class BuildingDegradationReclaimableMaterial
	{
		[Key]
		[JsonIgnore]
		public int BuildingDegradationReclaimableMaterialModelId { get; set; }

		[StringLength(8)]
		public string MaterialTicker { get; set; }
		public int MaterialCount { get; set; }

		[JsonIgnore]
		public int BuildingDegradationId { get; set; }

		[JsonIgnore]
		public virtual BuildingDegradation BuildingDegradation { get; set; }

		public void Validate()
		{
			Debug.Assert(!String.IsNullOrWhiteSpace(MaterialTicker) && MaterialTicker.Length <= 8);
		}
	}

	[Index(nameof(BuildingDegradationId), nameof(MaterialTicker), IsUnique = true)]
	public class BuildingDegradationRepairMaterial
	{
		[Key]
		[JsonIgnore]
		public int BuildingDegradationRepairMaterialId { get; set; }

		[StringLength(8)]
		public string MaterialTicker { get; set; }
		public int MaterialCount { get; set; }

		[JsonIgnore]
		public int BuildingDegradationId { get; set; }

		[JsonIgnore]
		public virtual BuildingDegradation BuildingDegradation { get; set; }

		public void Validate()
		{
			Debug.Assert(!String.IsNullOrWhiteSpace(MaterialTicker) && MaterialTicker.Length <= 8);
		}
	}
}
