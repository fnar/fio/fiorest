﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class ChatModel
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ChannelId { get; set; }

        public long CreationTime { get; set; }

        [StringLength(64)]
        public string DisplayName { get; set; }

        public long LastActivity { get; set; }

        [StringLength(16)]
        public string NaturalId { get; set; }

        [StringLength(8)]
        public string Type { get; set; }

        public int UserCount { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public virtual List<ChatMessage> Messages { get; set; } = new List<ChatMessage>();

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrWhiteSpace(ChannelId) && ChannelId.Length == 32 &&
                !String.IsNullOrWhiteSpace(DisplayName) && DisplayName.Length <= 64 &&
                LastActivity > 0 &&
                (NaturalId == null || NaturalId.Length <= 16) &&
                !String.IsNullOrWhiteSpace(Type) && Type.Length <= 8 &&
                !String.IsNullOrWhiteSpace(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default);

            Messages.ForEach(m => m.Validate());
        }
    }

    [Microsoft.EntityFrameworkCore.Index(nameof(ChatModelId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(Timestamp))]
    [Microsoft.EntityFrameworkCore.Index(nameof(MessageTimestamp))]
    public class ChatMessage
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string MessageId { get; set; }

        [StringLength(32)]
        public string MessageType { get; set; }

        [StringLength(32)]
        public string SenderId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }

        public string MessageText { get; set; }
        public long MessageTimestamp { get; set; }
        public bool MessageDeleted { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        [StringLength(32)]
        public string ChatModelId { get; set; }

        [JsonIgnore]
        public virtual ChatModel ChatModel { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(MessageId) && MessageId.Length == 32 &&
                !String.IsNullOrEmpty(MessageType) && MessageType.Length <= 32 &&
                !String.IsNullOrEmpty(SenderId) && (SenderId.Length == 32 || SenderId == "UnknownUserId") &&
                !String.IsNullOrEmpty(UserName) && UserName.Length <= 32 &&
                MessageTimestamp > 0 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default &&
                !String.IsNullOrEmpty(ChatModelId) && ChatModelId.Length == 32);
        }
    }
}
