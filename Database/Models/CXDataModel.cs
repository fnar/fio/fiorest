﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    [Microsoft.EntityFrameworkCore.Index(nameof(ExchangeCode))]
    [Microsoft.EntityFrameworkCore.Index(nameof(MaterialName))]
    [Microsoft.EntityFrameworkCore.Index(nameof(MaterialTicker))]
    [Microsoft.EntityFrameworkCore.Index(nameof(MaterialId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(ExchangeCode), nameof(MaterialId), IsUnique = true)]
    public class CXDataModel
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CXDataModelId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }

        [StringLength(64)]
        public string ExchangeName { get; set; }
        [StringLength(8)]
        public string ExchangeCode { get; set; }

        [StringLength(8)]
        public string Currency { get; set; }

        public double? Previous { get; set; }
        public double? Price { get; set; }
        public long? PriceTimeEpochMs { get; set; }

        public double? High { get; set; }
        public double? AllTimeHigh { get; set; }

        public double? Low { get; set; }
        public double? AllTimeLow { get; set; }

        public double? Ask { get; set; }
        public int? AskCount { get; set; }

        public double? Bid { get; set; }
        public int? BidCount { get; set; }

        public int? Supply { get; set; }
        public int? Demand { get; set; }

        public int? Traded { get; set; }

        public double? VolumeAmount { get; set; }

        public double? PriceAverage { get; set; }

        public double? NarrowPriceBandLow { get; set; }
        public double? NarrowPriceBandHigh { get; set; }

        public double? WidePriceBandLow { get; set; }
        public double? WidePriceBandHigh { get; set; }

        public double? MMBuy { get; set; }
        public double? MMSell { get; set; }

        public virtual List<CXBuyOrder> BuyingOrders { get; set; } = new List<CXBuyOrder>();
        public virtual List<CXSellOrder> SellingOrders { get; set; } = new List<CXSellOrder>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(CXDataModelId) && CXDataModelId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(ExchangeName) && ExchangeName.Length <= 64 &&
                !String.IsNullOrEmpty(ExchangeCode) && ExchangeCode.Length <= 8 &&
                !String.IsNullOrEmpty(Currency) && Currency.Length <= 8 &&
                (Previous == null || Previous >= 0.0) &&
                (Price == null || Price >= 0.0) &&
                (PriceTimeEpochMs == null || PriceTimeEpochMs > 0) &&
                (High == null || High >= 0.0) &&
                (AllTimeHigh == null || AllTimeHigh >= 0.0) &&
                (Low == null || Low >= 0.0) &&
                (AllTimeLow == null || AllTimeLow >= 0.0) &&
                (Ask == null || Ask >= 0.0) &&
                (AskCount == null || AskCount >= 0) &&
                (Bid == null || Bid >= 0.0) &&
                (BidCount == null || BidCount >= 0) &&
                (Supply == null || Supply >= 0) &&
                (Demand == null || Demand >= 0) &&
                (Traded == null || Traded >= 0) &&
                (VolumeAmount == null || VolumeAmount >= 0.0) &&
                (PriceAverage == null || PriceAverage >= 0.0) &&
                (NarrowPriceBandLow == null || NarrowPriceBandLow >= 0.0) &&
                (NarrowPriceBandHigh == null || NarrowPriceBandHigh >= 0.0) &&
                (WidePriceBandLow == null || WidePriceBandLow >= 0.0) &&
                (WidePriceBandHigh == null || WidePriceBandHigh >= 0.0) &&
                (MMBuy == null || MMBuy > 0.0) &&
                (MMSell == null || MMSell > 0.0) &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            BuyingOrders.ForEach(bo => bo.Validate());
            SellingOrders.ForEach(so => so.Validate());
        }
    }

    [Microsoft.EntityFrameworkCore.Index(nameof(OrderId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(CXDataModelId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(OrderId), nameof(CXDataModelId), IsUnique = true)]
    public class CXBuyOrder
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string OrderId { get; set; }

        [StringLength(32)]
        public string CompanyId { get; set; }

        [StringLength(64)]
        public string CompanyName { get; set; }

        [StringLength(8)]
        public string CompanyCode { get; set; }

        public int? ItemCount { get; set; }
        public double ItemCost { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string CXDataModelId { get; set; }

        [JsonIgnore]
        public virtual CXDataModel CXDataModel { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(OrderId) && OrderId.Length == 32 &&
                !String.IsNullOrEmpty(CompanyId) && CompanyId.Length == 32 &&
                !String.IsNullOrEmpty(CompanyName) && CompanyName.Length <= 64 &&
                !String.IsNullOrEmpty(CompanyCode) && CompanyCode.Length <= 8 &&
                (ItemCount == null || ItemCount > 0) &&
                ItemCost > 0.0 &&
                !String.IsNullOrEmpty(CXDataModelId) && CXDataModelId.Length == 32
                );
        }
    }

    [Microsoft.EntityFrameworkCore.Index(nameof(OrderId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(CXDataModelId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(OrderId), nameof(CXDataModelId), IsUnique = true)]
    public class CXSellOrder
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string OrderId { get; set; }

        [StringLength(32)]
        public string CompanyId { get; set; }

        [StringLength(64)]
        public string CompanyName { get; set; }

        [StringLength(8)]
        public string CompanyCode { get; set; }

        public int? ItemCount { get; set; }
        public double ItemCost { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string CXDataModelId { get; set; }

        [JsonIgnore]
        public virtual CXDataModel CXDataModel { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(OrderId) && OrderId.Length == 32 &&
                !String.IsNullOrEmpty(CompanyId) && CompanyId.Length == 32 &&
                !String.IsNullOrEmpty(CompanyName) && CompanyName.Length <= 64 &&
                !String.IsNullOrEmpty(CompanyCode) && CompanyCode.Length <= 8 &&
                (ItemCount == null || ItemCount > 0) &&
                ItemCost > 0.0 &&
                !String.IsNullOrEmpty(CXDataModelId) && CXDataModelId.Length == 32
                );
        }
    }

    [Microsoft.EntityFrameworkCore.Index(nameof(MaterialTicker))]
    [Microsoft.EntityFrameworkCore.Index(nameof(ExchangeCode))]
    [Microsoft.EntityFrameworkCore.Index(nameof(MaterialTicker), nameof(ExchangeCode), IsUnique = true)]
    public class CXPCData
    {
        [Key]
        [JsonIgnore]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CXPCDataId { get; set; } // BrokerId

        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(8)]
        public string ExchangeCode { get; set; }

        public virtual List<CXPCDataEntry> Entries { get; set; } = new List<CXPCDataEntry>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(CXPCDataId) && CXPCDataId.Length <= 32 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(ExchangeCode) && ExchangeCode.Length <= 8 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            Entries.ForEach(e => e.Validate());
        }
    }

    public class CXPCDataEntry
    {
        [Key]
        [JsonIgnore]
        [StringLength(128)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CXPCDataEntryId { get; set; } // CXPCDataId-Interval-Timestamp

        public string Interval { get; set; }

        public long DateEpochMs { get; set; }

        public double Open { get; set; }

        public double Close { get; set; }

        public double High { get; set; }

        public double Low { get; set; }

        public double Volume { get; set; }

        public int Traded { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string CXPCDataId { get; set; }

        [JsonIgnore]
        public virtual CXPCData CXPCData { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(CXPCDataEntryId) && CXPCDataEntryId.Length <= 64 &&
                DateEpochMs > 0 &&
                !String.IsNullOrEmpty(CXPCDataId) && CXPCDataId.Length <= 32
                );
        }
    }

    public class Station
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string StationId { get; set; }

        [StringLength(8)]
        public string NaturalId { get; set; }
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(32)]
        public string SystemId { get; set; }
        [StringLength(8)]
        public string SystemNaturalId { get; set; }
        [StringLength(32)]
        public string SystemName { get; set; }

        public long CommisionTimeEpochMs { get; set; }

        [StringLength(32)]
        public string ComexId { get; set; }
        [StringLength(64)]
        public string ComexName { get; set; }
        [StringLength(8)]
        public string ComexCode { get; set; }

        [StringLength(32)]
        public string WarehouseId { get; set; }

        [StringLength(32)]
        public string CountryId { get; set; }
        [StringLength(8)]
        public string CountryCode { get; set; }
        [StringLength(64)]
        public string CountryName { get; set; }

        public int CurrencyNumericCode { get; set; }
        [StringLength(8)]
        public string CurrencyCode { get; set; }
        [StringLength(32)]
        public string CurrencyName { get; set; }
        public int CurrencyDecimals { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(StationId) && StationId.Length == 32 &&
                !String.IsNullOrEmpty(NaturalId) && NaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(Name) && Name.Length <= 64 &&
                !String.IsNullOrEmpty(SystemId) && SystemId.Length == 32 &&
                !String.IsNullOrEmpty(SystemNaturalId) && SystemNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(SystemName) && SystemName.Length <= 32 &&
                CommisionTimeEpochMs > 0 &&
                !String.IsNullOrEmpty(ComexId) && ComexId.Length == 32 &&
                !String.IsNullOrEmpty(ComexName) && ComexName.Length <= 64 &&
                !String.IsNullOrEmpty(ComexCode) && ComexCode.Length <= 8 &&
                (WarehouseId == null || WarehouseId.Length == 32) &&
                !String.IsNullOrEmpty(CountryId) && CountryId.Length == 32 &&
                !String.IsNullOrEmpty(CountryCode) && CountryCode.Length <= 8 &&
                !String.IsNullOrEmpty(CountryName) && CountryName.Length <= 64 &&
                CurrencyNumericCode >= 0 &&
                !String.IsNullOrEmpty(CurrencyCode) && CurrencyCode.Length <= 8 &&
                !String.IsNullOrEmpty(CurrencyName) && CurrencyName.Length <= 32 &&
                CurrencyDecimals > 0 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32
                );
        }
    }
}
