﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(Ticker))]
    public class Building
    {
        public Building() { }

        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingId { get; set; }

        [StringLength(64)]
        public string Name { get; set; }
        [StringLength(8)]
        public string Ticker { get; set; }

        [StringLength(32)]
        public string Expertise { get; set; }

        public int Pioneers { get; set; }
        public int Settlers { get; set; }
        public int Technicians { get; set; }
        public int Engineers { get; set; }
        public int Scientists { get; set; }

        public int AreaCost { get; set; }

        public virtual List<BuildingCost> BuildingCosts { get; set; } = new List<BuildingCost>();
        public virtual List<BuildingRecipe> Recipes { get; set; } = new List<BuildingRecipe>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(BuildingId) && BuildingId.Length == 32 &&
                !String.IsNullOrEmpty(Name) && Name.Length <= 64 &&
                !String.IsNullOrEmpty(Ticker) && Ticker.Length <= 8 &&
                AreaCost > 0 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default);

            BuildingCosts.ForEach(bc => bc.Validate());
            Recipes.ForEach(r => r.Validate());
        }
    }

    public class BuildingCost
    {
        public BuildingCost() { }

        [Key]
        [JsonIgnore]
        [StringLength(40)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingCostId { get; set; }

        [StringLength(64)]
        public string CommodityName { get; set; }
        [StringLength(8)]
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string BuildingId { get; set; }

        [JsonIgnore]
        public virtual Building Building { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(BuildingCostId) && BuildingCostId.Length <= 40 &&
                !String.IsNullOrEmpty(CommodityName) && CommodityName.Length < 64 &&
                !String.IsNullOrEmpty(CommodityTicker) && CommodityTicker.Length < 8 &&
                Weight > 0.0 &&
                Volume > 0.0 &&
                Amount > 0 &&
                !String.IsNullOrWhiteSpace(BuildingId) && BuildingId.Length == 32);
        }
    }

    public class BuildingRecipeInput
    {
        public BuildingRecipeInput() { }

        [Key]
        [JsonIgnore]
        [StringLength(100)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingRecipeInputId { get; set; }

        [StringLength(64)]
        public string CommodityName { get; set; }
        [StringLength(8)]
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(100)]
        public string BuildingRecipeId { get; set; }

        [JsonIgnore]
        public virtual BuildingRecipe BuildingRecipe { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(BuildingRecipeInputId) && BuildingRecipeInputId.Length <= 100 &&
                !String.IsNullOrEmpty(CommodityName) && CommodityName.Length <= 64 &&
                !String.IsNullOrEmpty(CommodityTicker) && CommodityTicker.Length <= 8 &&
                Weight > 0.0 &&
                Volume > 0.0 &&
                Amount > 0 &&
                !String.IsNullOrWhiteSpace(BuildingRecipeId) && BuildingRecipeId.Length <= 100);
        }
    }

    public class BuildingRecipeOutput
    {
        public BuildingRecipeOutput() { }

        [Key]
        [JsonIgnore]
        [StringLength(100)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingRecipeOutputId { get; set; }

        [StringLength(64)]
        public string CommodityName { get; set; }
        [StringLength(8)]
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(100)]
        public string BuildingRecipeId { get; set; }

        [JsonIgnore]
        public virtual BuildingRecipe BuildingRecipe { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(BuildingRecipeOutputId) && BuildingRecipeOutputId.Length <= 100 &&
                !String.IsNullOrEmpty(CommodityName) && CommodityName.Length <= 64 &&
                !String.IsNullOrEmpty(CommodityTicker) && CommodityTicker.Length <= 8 &&
                Weight > 0.0 &&
                Volume > 0.0 &&
                Amount > 0 &&
                !String.IsNullOrWhiteSpace(BuildingRecipeId) && BuildingRecipeId.Length <= 100);
        }
    }

    public class BuildingRecipe
    {
        public BuildingRecipe() { }

        [Key]
        [StringLength(100)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingRecipeId { get; set; }

        public virtual List<BuildingRecipeInput> Inputs { get; set; } = new List<BuildingRecipeInput>();
        public virtual List<BuildingRecipeOutput> Outputs { get; set; } = new List<BuildingRecipeOutput>();
        public int DurationMs { get; set; }
        [StringLength(64)]
        public string RecipeName { get; set; }

        [StringLength(100)]
        public string StandardRecipeName { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string BuildingId { get; set; }

        [JsonIgnore]
        public virtual Building Building { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrWhiteSpace(BuildingRecipeId) && BuildingRecipeId.Length <= 100 &&
                DurationMs > 0 &&
                !String.IsNullOrWhiteSpace(RecipeName) && RecipeName.Length <= 64 &&
                !String.IsNullOrEmpty(StandardRecipeName) && RecipeName.Length <= 100 &&
                !String.IsNullOrWhiteSpace(BuildingId) && BuildingId.Length == 32);

            Inputs.ForEach(i => i.Validate());
            Outputs.ForEach(o => o.Validate());
        }

        public string GetRecipeDescription()
        {
            var inputs = Inputs.OrderBy(i => i.CommodityTicker).Select(i => $"{i.Amount}x{i.CommodityTicker}");
            var outputs = Outputs.OrderBy(o => o.CommodityTicker).Select(o => $"{o.Amount}x{o.CommodityTicker}");
            return $"{Building.Ticker}:{String.Join('-', inputs)}=>{String.Join('-', outputs)}";
        }
    }
}