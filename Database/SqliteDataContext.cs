﻿using System;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Database
{
    public class SqliteDataContext : PRUNDataContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // Uncomment (todo: implement debug logging) to log SQL queries.
            PRUNDataContext.LogDebug(options);
            options
                .UseSqlite(
                    Globals.ConnectionString,
#if DEBUG
                    opts => opts.CommandTimeout(300)
#else
                    opts => opts.CommandTimeout(30)
#endif
                ).UseLazyLoadingProxies();
        }
    }
}
