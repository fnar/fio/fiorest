using System;
using System.Threading;

namespace FIORest
{
	public class GlobalMutex : IDisposable
	{
		private bool bHasHandle = false;
		private Mutex globalMutex;

		/// <summary>
		/// A GlobalMutex object 
		/// </summary>
		/// <param name="mutexId">A suffix to the GlobalMutex</param>
		/// <param name="timeoutMs">Timeout in milliseconds.  Specify negative value for infinite</param>
		public GlobalMutex( string guid, string mutexIdSuffix = null, int timeoutMs = 5000 )
		{
			string mutexId = @"Global\" + guid;

			if ( !String.IsNullOrEmpty( mutexIdSuffix ) )
			{
				mutexId += $"-{mutexIdSuffix}";
			}

			globalMutex = new Mutex( false, mutexId );

			try
			{
				if ( timeoutMs < 0 )
					bHasHandle = globalMutex.WaitOne( Timeout.Infinite, false );
				else
					bHasHandle = globalMutex.WaitOne( timeoutMs, false );
			}
			catch ( AbandonedMutexException )
			{
				bHasHandle = true;
			}
		}

		public bool Acquired()
		{
			return bHasHandle;
		}

		public void Dispose()
		{
			if ( globalMutex != null )
			{
				if ( bHasHandle )
				{
					globalMutex.ReleaseMutex();
					bHasHandle = false;
				}

				globalMutex.Close();
			}
		}
	}
}