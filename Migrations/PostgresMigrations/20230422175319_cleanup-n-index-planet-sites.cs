﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class cleanupnindexplanetsites : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM \"PlanetSites\" a USING \"PlanetSites\" b WHERE a.\"PlanetSiteId\" < b.\"PlanetSiteId\" AND a.\"PlanetId\" = b.\"PlanetId\" AND a.\"PlotId\" = b.\"PlotId\";");

            migrationBuilder.CreateIndex(
                name: "IX_PlanetSites_PlanetId_PlotId",
                table: "PlanetSites",
                columns: new[] { "PlanetId", "PlotId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PlanetSites_PlanetId_PlotId",
                table: "PlanetSites");
        }
    }
}
