﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class UpdateCXPC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDataEpochMs",
                table: "CXPCData");

            migrationBuilder.RenameColumn(
                name: "TimeEpochMs",
                table: "CXPCDataEntries",
                newName: "DateEpochMs");

            migrationBuilder.RenameColumn(
                name: "StartDataEpochMs",
                table: "CXPCData",
                newName: "FromEpochMs");

            migrationBuilder.AlterColumn<string>(
                name: "CXPCDataEntryId",
                table: "CXPCDataEntries",
                type: "character varying(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64);

            migrationBuilder.AddColumn<double>(
                name: "High",
                table: "CXPCDataEntries",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Interval",
                table: "CXPCDataEntries",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Low",
                table: "CXPCDataEntries",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateIndex(
                name: "IX_CXPCData_ExchangeCode",
                table: "CXPCData",
                column: "ExchangeCode");

            migrationBuilder.CreateIndex(
                name: "IX_CXPCData_MaterialTicker",
                table: "CXPCData",
                column: "MaterialTicker");

            migrationBuilder.CreateIndex(
                name: "IX_CXPCData_MaterialTicker_ExchangeCode",
                table: "CXPCData",
                columns: new[] { "MaterialTicker", "ExchangeCode" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CXPCData_ExchangeCode",
                table: "CXPCData");

            migrationBuilder.DropIndex(
                name: "IX_CXPCData_MaterialTicker",
                table: "CXPCData");

            migrationBuilder.DropIndex(
                name: "IX_CXPCData_MaterialTicker_ExchangeCode",
                table: "CXPCData");

            migrationBuilder.DropColumn(
                name: "High",
                table: "CXPCDataEntries");

            migrationBuilder.DropColumn(
                name: "Interval",
                table: "CXPCDataEntries");

            migrationBuilder.DropColumn(
                name: "Low",
                table: "CXPCDataEntries");

            migrationBuilder.RenameColumn(
                name: "DateEpochMs",
                table: "CXPCDataEntries",
                newName: "TimeEpochMs");

            migrationBuilder.RenameColumn(
                name: "FromEpochMs",
                table: "CXPCData",
                newName: "StartDataEpochMs");

            migrationBuilder.AlterColumn<string>(
                name: "CXPCDataEntryId",
                table: "CXPCDataEntries",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(128)",
                oldMaxLength: 128);

            migrationBuilder.AddColumn<long>(
                name: "EndDataEpochMs",
                table: "CXPCData",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
