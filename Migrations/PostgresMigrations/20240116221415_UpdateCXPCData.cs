﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class UpdateCXPCData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FromEpochMs",
                table: "CXPCData");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "FromEpochMs",
                table: "CXPCData",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
