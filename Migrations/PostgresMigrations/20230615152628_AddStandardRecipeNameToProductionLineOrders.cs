﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class AddStandardRecipeNameToProductionLineOrders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StandardRecipeName",
                table: "ProductionLineOrders",
                type: "character varying(100)",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StandardRecipeName",
                table: "ProductionLineOrders");
        }
    }
}
