﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class UpdateCXPCData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FromEpochMs",
                table: "CXPCData");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "FromEpochMs",
                table: "CXPCData",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
