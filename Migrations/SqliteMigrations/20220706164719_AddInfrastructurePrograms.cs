﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddInfrastructurePrograms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InfrastructurePrograms",
                columns: table => new
                {
                    InfrastructureProgramId = table.Column<string>(type: "TEXT", maxLength: 40, nullable: false),
                    Number = table.Column<int>(type: "INTEGER", nullable: false),
                    StartTimestampEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    EndTimestampEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    Category = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Program = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    InfrastructureId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructurePrograms", x => x.InfrastructureProgramId);
                    table.ForeignKey(
                        name: "FK_InfrastructurePrograms_Infrastructures_InfrastructureId",
                        column: x => x.InfrastructureId,
                        principalTable: "Infrastructures",
                        principalColumn: "InfrastructureId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructurePrograms_InfrastructureId",
                table: "InfrastructurePrograms",
                column: "InfrastructureId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InfrastructurePrograms");
        }
    }
}
