﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddAbilityToAddGroupsAsPermissions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GroupNameAndId",
                table: "PermissionAllowances",
                type: "TEXT",
                maxLength: 42,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GroupModelAdmin",
                columns: table => new
                {
                    GroupModelAdminId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GroupAdminUserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    GroupModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupModelAdmin", x => x.GroupModelAdminId);
                    table.ForeignKey(
                        name: "FK_GroupModelAdmin_GroupModels_GroupModelId",
                        column: x => x.GroupModelId,
                        principalTable: "GroupModels",
                        principalColumn: "GroupModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupModelAdmin_GroupModelId",
                table: "GroupModelAdmin",
                column: "GroupModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupModelAdmin");

            migrationBuilder.DropColumn(
                name: "GroupNameAndId",
                table: "PermissionAllowances");
        }
    }
}
