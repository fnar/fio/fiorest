﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class UpdatePlanetData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GovernorCorporationCode",
                table: "Stations");

            migrationBuilder.DropColumn(
                name: "GovernorCorporationId",
                table: "Stations");

            migrationBuilder.DropColumn(
                name: "GovernorCorporationName",
                table: "Stations");

            migrationBuilder.DropColumn(
                name: "GovernorId",
                table: "Stations");

            migrationBuilder.DropColumn(
                name: "GovernorUserName",
                table: "Stations");

            migrationBuilder.DropColumn(
                name: "CollectorCode",
                table: "PlanetDataModels");

            migrationBuilder.DropColumn(
                name: "CollectorId",
                table: "PlanetDataModels");

            migrationBuilder.DropColumn(
                name: "CollectorName",
                table: "PlanetDataModels");

            migrationBuilder.DropColumn(
                name: "GovernorCorporationCode",
                table: "PlanetDataModels");

            migrationBuilder.DropColumn(
                name: "GovernorCorporationId",
                table: "PlanetDataModels");

            migrationBuilder.DropColumn(
                name: "GovernorCorporationName",
                table: "PlanetDataModels");

            migrationBuilder.DropColumn(
                name: "GovernorId",
                table: "PlanetDataModels");

            migrationBuilder.RenameColumn(
                name: "GovernorUserName",
                table: "PlanetDataModels",
                newName: "GoverningEntity");

            migrationBuilder.AddColumn<double>(
                name: "EstablishmentFee",
                table: "PlanetDataModels",
                type: "REAL",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstablishmentFee",
                table: "PlanetDataModels");

            migrationBuilder.RenameColumn(
                name: "GoverningEntity",
                table: "PlanetDataModels",
                newName: "GovernorUserName");

            migrationBuilder.AddColumn<string>(
                name: "GovernorCorporationCode",
                table: "Stations",
                type: "TEXT",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorCorporationId",
                table: "Stations",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorCorporationName",
                table: "Stations",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorId",
                table: "Stations",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorUserName",
                table: "Stations",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CollectorCode",
                table: "PlanetDataModels",
                type: "TEXT",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CollectorId",
                table: "PlanetDataModels",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CollectorName",
                table: "PlanetDataModels",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorCorporationCode",
                table: "PlanetDataModels",
                type: "TEXT",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorCorporationId",
                table: "PlanetDataModels",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorCorporationName",
                table: "PlanetDataModels",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorId",
                table: "PlanetDataModels",
                type: "TEXT",
                maxLength: 32,
                nullable: true);
        }
    }
}
