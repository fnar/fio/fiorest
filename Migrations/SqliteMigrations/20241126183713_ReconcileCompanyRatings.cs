﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class ReconcileCompanyRatings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivityRating",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "ReliabilityRating",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "StabilityRating",
                table: "Companies");

            migrationBuilder.AddColumn<int>(
                name: "ContractCount",
                table: "Companies",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContractCount",
                table: "Companies");

            migrationBuilder.AddColumn<string>(
                name: "ActivityRating",
                table: "Companies",
                type: "TEXT",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReliabilityRating",
                table: "Companies",
                type: "TEXT",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StabilityRating",
                table: "Companies",
                type: "TEXT",
                maxLength: 8,
                nullable: true);
        }
    }
}
