﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.Channel.Data
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string channelId { get; set; }
        public string type { get; set; }
        public string naturalId { get; set; }
        public string displayName { get; set; }
        public StdTimestamp creationTime { get; set; }
        public StdTimestamp lastActivity { get; set; }
        public int userCount { get; set; }
        public int maxMessageLength { get; set; }
        public Permissions permissions { get; set; }
        public object[] bans { get; set; }
    }

    public class Permissions
    {
        public bool add { get; set; }
        public bool leave { get; set; }
    }
}
