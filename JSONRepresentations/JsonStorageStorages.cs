﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.StorageStorages
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Store[] stores { get; set; }
    }

    public class Store
    {
        public string id { get; set; }
        public string addressableId { get; set; }
        public string name { get; set; }
        public float weightLoad { get; set; }
        public int weightCapacity { get; set; }
        public float volumeLoad { get; set; }
        public int volumeCapacity { get; set; }
        public Item[] items { get; set; }
        public bool @fixed { get; set; }
        public bool tradeStore { get; set; }
        public int rank { get; set; }
        public bool locked { get; set; }
        public string type { get; set; }
    }

    public class Item
    {
        public Quantity quantity { get; set; }
        public string id { get; set; }
        public string type { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Quantity
    {
        public StdPrice value { get; set; }
        public StdMaterial material { get; set; }
        public int amount { get; set; }
    }
}
