﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.WorldMaterialCategories
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Category[] categories { get; set; }
    }

    public class Category
    {
        public object[] children { get; set; }
        public string name { get; set; }
        public string id { get; set; }
        public StdMaterial[] materials { get; set; }
    }
}
