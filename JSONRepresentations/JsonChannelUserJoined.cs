﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.Channel.UserJoined
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string messageId { get; set; }
        public User1 user { get; set; }
        public StdTimestamp time { get; set; }
        public string channelId { get; set; }
    }

    public class User1
    {
        public StdUser user { get; set; }
    }
}
