﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ComexBrokerPrices
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string brokerId { get; set; }
        public From from { get; set; }
        public Price[] prices { get; set; }
    }

    public class From
    {
        public long timestamp { get; set; }
    }

    public class Price
    {
        public string interval { get; set; }
        public Price1[] prices { get; set; }
    }

    public class Price1
    {
        public long date { get; set; }
        public float open { get; set; }
        public float close { get; set; }
        public float high { get; set; }
        public float low { get; set; }
        public float volume { get; set; }
        public int traded { get; set; }
    }
}
