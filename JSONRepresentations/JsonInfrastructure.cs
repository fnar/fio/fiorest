﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.Infrastructure
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public Report[] reports { get; set; }
        public Infrastructure[] infrastructure { get; set; }
        public string id { get; set; }
    }

    public class Report
    {
        public StdTimestamp time { get; set; }
        public int simulationPeriod { get; set; }
        public bool explorersGraceEnabled { get; set; }
        public PopulationQuantity nextPopulation { get; set; }
        public PopulationQuantity populationDifference { get; set; }
        public PopulationQuantity populationChange { get; set; }
        public PopulationQuantity populationShift { get; set; }
        public PopulationRate averageHappiness { get; set; }
        public PopulationRate unemploymentRate { get; set; }
        public PopulationQuantity openJobs { get; set; }
        public Needfulfillment needFulfillment { get; set; }
        public Needfulfillment1[] needFulfillments { get; set; }
    }

    public class PopulationQuantity
    {
        public int PIONEER { get; set; }
        public int SETTLER { get; set; }
        public int TECHNICIAN { get; set; }
        public int ENGINEER { get; set; }
        public int SCIENTIST { get; set; }
    }

    public class PopulationRate
    {
        public float PIONEER { get; set; }
        public float SETTLER { get; set; }
        public float TECHNICIAN { get; set; }
        public float ENGINEER { get; set; }
        public float SCIENTIST { get; set; }
    }

    public class Needfulfillment
    {
        public float LIFE_SUPPORT { get; set; }
        public float SAFETY { get; set; }
        public float HEALTH { get; set; }
        public float COMFORT { get; set; }
        public float CULTURE { get; set; }
        public float EDUCATION { get; set; }
    }

    public class Needfulfillment1
    {
        public string infrastructureType { get; set; }
        public string needType { get; set; }
        public float fulfillment { get; set; }
    }

    public class Infrastructure
    {
        public string type { get; set; }
        public string ticker { get; set; }
        public string projectId { get; set; }
        public string projectName { get; set; }
        public int level { get; set; }
        public int activeLevel { get; set; }
        public int currentLevel { get; set; }
        public float upkeepStatus { get; set; }
        public float upgradeStatus { get; set; }
    }
}
