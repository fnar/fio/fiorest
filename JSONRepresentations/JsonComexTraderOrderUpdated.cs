﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.JsonComexTraderOrderUpdated
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string id { get; set; }
        public StdExchange exchange { get; set; }
        public string brokerId { get; set; }
        public string type { get; set; }
        public StdMaterial material { get; set; }
        public int amount { get; set; }
        public int initialAmount { get; set; }
        public StdPrice limit { get; set; }
        public string status { get; set; }
        public StdTimestamp created { get; set; }
        public Trade[] trades { get; set; }
    }

    public class Trade
    {
        public string id { get; set; }
        public int amount { get; set; }
        public StdPrice price { get; set; }
        public StdTimestamp time { get; set; }
        public StdCompany partner { get; set; }
    }

}
