﻿using Newtonsoft.Json;
using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.JsonPlanetSites
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        [JsonProperty("body")]
        public Site[] sites { get; set; }
        public string[] path { get; set; }
    }

    public class Site
    {
        public string planetId { get; set; }
        public string ownerId { get; set; }
        public StdCompany entity { get; set; }
        public string type { get; set; }
        public int plot { get; set; }
        public string id { get; set; }
    }
}
