﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ComexExchangeList
{
	public class Rootobject
	{
		public string messageType { get; set; }
		public Payload payload { get; set; }
	}

	public class Payload
	{
		public string actionId { get; set; }
		public int status { get; set; }
		public Message message { get; set; }
	}

	public class Message
	{
		public string messageType { get; set; }
		public Payload1 payload { get; set; }
	}

	public class Payload1
	{
		public Body[] body { get; set; }
		public string[] path { get; set; }
	}

	public class Body
	{
		public string name { get; set; }
		public string code { get; set; }
		public StdCompany _operator { get; set; }
		public Currency currency { get; set; }
		public StdAddress address { get; set; }
		public string id { get; set; }
	}

	public class Currency
	{
		public int numericCode { get; set; }
		public string code { get; set; }
		public string name { get; set; }
		public int decimals { get; set; }
	}
}
