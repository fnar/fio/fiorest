﻿namespace FIORest.JSONRepresentations.ProductionLineRemoved
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string siteId { get; set; }
        public string productionLineId { get; set; }
    }
}
