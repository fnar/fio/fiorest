﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.PlanetData
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string planetId { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public StdUser namer { get; set; }
        public StdTimestamp namingDate { get; set; }
        public bool nameable { get; set; }
        public object[] celestialBodies { get; set; }
        public StdAddress address { get; set; }
        public Data data { get; set; }
        public Buildoptions buildOptions { get; set; }
        public Project[] projects { get; set; }
        public Country country { get; set; }
        public StdUser governor { get; set; }
        public string governingEntity { get; set; }
        public StdCurrency currency { get; set; }
        public Localrules localRules { get; set; }
        public string populationId { get; set; }
        public string cogcId { get; set; }
        public string id { get; set; }
    }

    public class Data
    {
        public float gravity { get; set; }
        public float magneticField { get; set; }
        public float mass { get; set; }
        public float massEarth { get; set; }
        public Orbit orbit { get; set; }
        public int orbitIndex { get; set; }
        public float pressure { get; set; }
        public float radiation { get; set; }
        public float radius { get; set; }
        public Resource[] resources { get; set; }
        public float sunlight { get; set; }
        public bool surface { get; set; }
        public float temperature { get; set; }
        public int plots { get; set; }
        public float fertility { get; set; }
    }

    public class Orbit
    {
        public long semiMajorAxis { get; set; }
        public float eccentricity { get; set; }
        public float inclination { get; set; }
        public int rightAscension { get; set; }
        public int periapsis { get; set; }
    }

    public class Resource
    {
        public string materialId { get; set; }
        public string type { get; set; }
        public float factor { get; set; }
    }

    public class Center
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

    public class Buildoptions
    {
        public Option[] options { get; set; }
    }

    public class Option
    {
        public string siteType { get; set; }
        public Billofmaterial billOfMaterial { get; set; }
    }

    public class Billofmaterial
    {
        public StdMaterialAmount[] quantities { get; set; }
    }

    // Faction country matches format of stdcompany for now.
    public class Country : StdCompany { }

    public class StdCollector
    {
        public string _proxy_key { get; set; }
        public string _type { get; set; }
        public StdCurrency currency { get; set; }
    }

    public class Localrules
    {
        public StdCollector collector { get; set; }
        public string[] governingEntityTypes { get; set; }
        public Productionfees productionFees { get; set; }
        public Localmarketfee localMarketFee { get; set; }
        public Warehousefee warehouseFee { get; set; }
        public Establishmentfee siteEstablishmentFee { get; set; }
    }

    public class Productionfees
    {
        public Fee[] fees { get; set; }
    }

    public class Fee
    {
        public string category { get; set; }
        public string workforceLevel { get; set; }
        public StdPrice fee { get; set; }
    }

    public class Localmarketfee
    {
        public int _base { get; set; }
        public float timeFactor { get; set; }
    }

    public class Establishmentfee
    {
        public int fee { get; set; }
    }

    public class Warehousefee
    {
        public int fee { get; set; }
    }

    public class Project
    {
        public string type { get; set; }
        public string entityId { get; set; }
    }

}
