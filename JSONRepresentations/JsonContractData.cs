﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.JsonContractData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Contract[] contracts { get; set; }
    }

    public class Contract
    {
        public string id { get; set; }
        public string localId { get; set; } // `CONT {localId}` buffer
        public StdTimestamp date { get; set; }
        public string party { get; set; } // `PROVIDER` or `CUSTOMER`
        public StdCompany partner { get; set; }
        public string status { get; set; }
        public Condition[] conditions { get; set; }
        public StdTimestamp extensionDeadline { get; set; }
        public bool canExtend { get; set; }
        public bool canRequestTermination { get; set; }
        public bool terminationSent { get; set; }
        public bool terminationReceived { get; set; }
        public StdTimestamp dueDate { get; set; }
        public string name { get; set; }
        public string preamble { get; set; }
    }

    public class Condition
    {
        public StdAddress address { get; set; }
        public StdMaterialAmount quantity { get; set; } // null if shipment
        public double weight { get; set; }
        public double volume { get; set; }
        public string blockId { get; set; }
        public string type { get; set; }
        public string id { get; set; }
        public string party { get; set; }
        public int index { get; set; }
        public string status { get; set; }
        public string[] dependencies { get; set; }
        public StdTimestamp deadline { get; set; }
        public StdPrice amount { get; set; }
        public StdAddress destination { get; set; }
        public string shipmentItemId { get; set; }
        public StdMaterialAmount pickedUp { get; set; }
    }
}
