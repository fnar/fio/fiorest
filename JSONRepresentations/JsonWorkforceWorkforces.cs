﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.WorkforceWorkforces
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public StdAddress address { get; set; }
        public string siteId { get; set; }
        public Workforce[] workforces { get; set; }
    }

    public class Workforce
    {
        public string level { get; set; }
        public int population { get; set; }
        public int reserve { get; set; }
        public int capacity { get; set; }
        public int required { get; set; }
        public float satisfaction { get; set; }
        public Need[] needs { get; set; }
    }

    public class Need
    {
        public string category { get; set; }
        public bool essential { get; set; }
        public StdMaterial material { get; set; }
        public float satisfaction { get; set; }
        public float unitsPerInterval { get; set; }
        public float unitsPer100 { get; set; }
    }
}
