﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.WorldReactorData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string id { get; set; }
        public string name { get; set; }
        public string ticker { get; set; }
        public int areaCost { get; set; }
        public string expertise { get; set; }
        public StdMaterialAmount[] buildingCosts { get; set; }
        public Workforcecapacity[] workforceCapacities { get; set; }
        public Recipe[] recipes { get; set; }
    }

    public class Workforcecapacity
    {
        public string level { get; set; }
        public int capacity { get; set; }
    }

    public class Recipe
    {
        public StdMaterialAmount[] inputs { get; set; }
        public StdMaterialAmount[] outputs { get; set; }
        public Duration duration { get; set; }
    }

    public class Duration
    {
        public int millis { get; set; }
    }
}
