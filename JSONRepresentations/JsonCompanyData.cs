﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.CompanyData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string countryId { get; set; }
        public Currency ownCurrency { get; set; }
        public Currencyaccount[] currencyAccounts { get; set; }
        public string startingProfile { get; set; }
        public StdAddress startingLocation { get; set; }
        public Ratingreport ratingReport { get; set; }
        public Headquarters headquarters { get; set; }
    }

    public class Currency
    {
        public int numericCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int decimals { get; set; }
    }

    public class Ratingreport
    {
        public string overallRating { get; set; }
        public Subrating[] subRatings { get; set; }
    }

    public class Subrating
    {
        public string score { get; set; }
        public string rating { get; set; }
    }

    public class Headquarters
    {
        public StdAddress address { get; set; }
        public int level { get; set; }
        public int basePermits { get; set; }
        public int usedBasePermits { get; set; }
        public Inventory inventory { get; set; }
        public int additionalBasePermits { get; set; }
        public int additionalProductionQueueSlots { get; set; }
        public StdTimestamp nextRelocationTime { get; set; }
        public bool relocationLocked { get; set; }
        public EfficiencyGains[] efficiencyGains { get; set; }
        public EfficiencyGains[] efficiencyGainsNextLevel { get; set; }
    }

    public class Inventory
    {
        public Item[] items { get; set; }
    }

    public class Item
    {
        public StdMaterial material { get; set; }
        public int amount { get; set; }
        public int limit { get; set; }
    }

    public class EfficiencyGains
    {
        public string category { get; set; }
        public float gain { get; set; }
    }

    public class Currencyaccount
    {
        public string category { get; set; }
        public int type { get; set; }
        public int number { get; set; }
        public StdPrice bookBalance { get; set; }
        public StdPrice currencyBalance { get; set; }
    }

}
