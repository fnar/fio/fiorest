﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ComexBrokerData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string id { get; set; }
        public string ticker { get; set; }
        public StdExchange exchange { get; set; }
        public StdAddress address { get; set; }
        public StdCurrency currency { get; set; }
        public StdMaterial material { get; set; }
#nullable enable
        public StdPrice? previous { get; set; }
#nullable disable
        public StdPrice price { get; set; }
        public StdTimestamp priceTime { get; set; }
        public StdPrice high { get; set; }
        public StdPrice allTimeHigh { get; set; }
        public StdPrice low { get; set; }
        public StdPrice allTimeLow { get; set; }
        public Operation ask { get; set; }
        public Operation bid { get; set; }
        public int supply { get; set; }
        public int demand { get; set; }
        public int traded { get; set; }
        public StdPrice volume { get; set; }
        public StdPrice priceAverage { get; set; }
        public PriceBand narrowPriceBand { get; set; }
        public PriceBand widePriceBand { get; set; }
        public Order[] sellingOrders { get; set; }
        public Order[] buyingOrders { get; set; }
    }

    public class Operation
    {
        public StdPrice price { get; set; }
        public int amount { get; set; }
    }

    public class PriceBand
    {
        public float low { get; set; }
        public float high { get; set; }
    }

    public class Order
    {
        public string id { get; set; }
        public StdCompany trader { get; set; }
        public int? amount { get; set; }
        public StdPrice limit { get; set; }
    }
    
}
