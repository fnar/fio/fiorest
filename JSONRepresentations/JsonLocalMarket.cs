﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.LocalMarket
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body[] body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string localMarketId { get; set; }
        public int naturalId { get; set; }
        public string status { get; set; }
        public StdCompany creator { get; set; }
        public string type { get; set; }
        public StdAddress address { get; set; }
        public StdMaterialAmount quantity { get; set; }
        public StdPrice price { get; set; }
        public int advice { get; set; }
        public StdTimestamp creationTime { get; set; }
        public string minimumRating { get; set; }
        public StdTimestamp expiry { get; set; }
        public string id { get; set; }
        public StdAddress origin { get; set; }
        public StdAddress destination { get; set; }
        public float cargoWeight { get; set; }
        public float cargoVolume { get; set; }
    }

}
