﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.JsonComexTraderOrders
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Order[] orders { get; set; }
    }

    public class Order
    {
        public string id { get; set; }
        public StdExchange exchange { get; set; }
        public string brokerId { get; set; }
        public string type { get; set; }
        public StdMaterial material { get; set; }
        public int amount { get; set; }
        public int initialAmount { get; set; }
        public StdPrice limit { get; set; }
        public string status { get; set; }
        public StdTimestamp created { get; set; }
        public Trade[] trades { get; set; }
    }

    public class Trade
    {
        public string id { get; set; }
        public int amount { get; set; }
        public StdPrice price { get; set; }
        public StdTimestamp time { get; set; }
        public StdCompany partner { get; set; }
    }
}
