﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.InfrastructureProgram
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public StdAddress address { get; set; }
        public StdCompany _operator { get; set; }
        public StdTimestamp completionDate { get; set; }
        public string currentTerm { get; set; }
        public string upcomingTerm { get; set; }
        public string[] previousTerms { get; set; }
        public string populationId { get; set; }
        public Programs programs { get; set; }
        public string id { get; set; }
    }

    public class Programs
    {
        public Program currentProgram { get; set; }
        public Program nextProgram { get; set; }
        public Program[] pastPrograms { get; set; }
    }

    public class Program
    {
        public int number { get; set; }
        public StdTimestamp start { get; set; }
        public StdTimestamp end { get; set; }
        public string category { get; set; }
        public string program { get; set; }
    }
}
