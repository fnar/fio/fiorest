﻿using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;

namespace FIORest.JSONRepresentations.Common
{
    public class StdAddress
    {
        public StdAddressLine[] lines { get; set; }
    }

    public class StdAddressLine
    {
        public StdPlanetEntity entity { get; set; }
        public string type { get; set; }
#nullable enable
        public StdOrbit? orbit { get; set; }
#nullable disable
    }

    public class StdPlanetEntity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class StdOrbit
    {
        public float semiMajorAxis { get; set; }
        public float eccentricity { get; set; }
        public float inclination { get; set; }
        public float rightAscension { get; set; }
        public float periapsis { get; set; }
    }

    // In many places, an "entity" is a company, country, faction, or exchange.
    // Can't guarantee it'll always be that way, so we'll create some variants of the name.
    public class StdEntity
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }
    // Exchanges are currently the same fields as companies, but that's
    // not guaranteed to always be the case.
    public class StdCompany : StdEntity { }
    public class StdCorporation : StdEntity { }
    public class StdExchange : StdEntity { }

    public class StdMaterial
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
        public bool? resource { get; set; }
    }

    public class StdMaterialAmount
    {
        public StdMaterial material { get; set; }
        public int amount { get; set; }
    }

    public class StdCurrency
    {
        public int numericCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int decimals { get; set; }
    }

    // StdPrice is an amount in a particular currency.
    // Strange, but accurate way to think about it.
    public class StdPrice
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class StdDuration
    {
        public long millis { get; set; }
    }

    public class StdTimestamp
    {
        public long timestamp { get; set; }
    }

    public class StdUser
    {
        public string id { get; set; }
        public string username { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

}

