﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FIORest
{
    public enum Method
    {
        GET,
        POST,
        PUT
    }
    
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class SharedRateLimitAttribute : Attribute
    {
        public SharedRateLimitAttribute(string EndPoint, Method Method) 
        {
            if (!EndPoint.StartsWith("/"))
                EndPoint = "/" + EndPoint;

            if (EndPoint.EndsWith("/"))
                EndPoint = EndPoint.Substring(0, EndPoint.Length - 1);
            
            this.EndPoint = EndPoint;

            switch(Method)
            {
                case Method.GET:
                    this.Method = "GET";
                    break;
                case Method.POST:
                    this.Method = "POST";
                    break;
                case Method.PUT:
                    this.Method = "PUT";
                    break;
            }
        }

        public string EndPoint
        {
            get; private set;
        }

        public string Method
        {
            get; private set;
        }

        public static HashSet<string> SharedRateLimits
        {
            get
            {
                if (_SharedRateLimits == null) 
                {
                    _SharedRateLimits = AppDomain.CurrentDomain.GetAssemblies()
                        .SelectMany(assembly => assembly.GetTypes()                                         // Get all types from the assembly
                        .Select(typ => typ.GetCustomAttributes(typeof(SharedRateLimitAttribute), true)))    // Search each type for the attribute
                        .Where(attr => attr != null && attr.Any())                                          // Cull any that don't have the attribute
                        .SelectMany(attr => attr)                                                           // Collapse the object[object[object] into an object[object]
                        .Select(attr => (SharedRateLimitAttribute)attr)                                     // Cast each object to a SharedRateLimitAttribute
                        .Select(attr => $"{attr.Method}-{attr.EndPoint.ToUpper()}")                         // Generate a string from the Method-Endpoint
                        .ToHashSet();                                                                       // HashSet it
                }


                return _SharedRateLimits;
            }
        }
        private static HashSet<string> _SharedRateLimits = null;
    }
}
