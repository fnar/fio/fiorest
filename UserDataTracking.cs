﻿#define USING_USER_DATA_TRACKING

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Nancy;
using Nancy.Extensions;
using Nancy.IO;

using FIORest.Authentication;
using FIORest.Database;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace FIORest
{
    public static class UserDataTracking
    {
        public static bool IsEnabled =
#if USING_USER_DATA_TRACKING
            true;
#else
            false;
#endif // USING_USER_DATA_TRACKING

        // It's strange to use a dictionary in this instance where we ignore the value, but it makes things _waaay_ easier for concurrency purposes
        // don't have to lock a list on write/read, it's all done for us.  Hackish, yes, but I don't know of any other good concurrent objects for us to use in this instance.
        // I'm sure it exists, though
        private static ConcurrentDictionary<string, int> UsersToTrack = new ConcurrentDictionary<string, int>();

        public static void AddUser(string username)
        {
            UsersToTrack.TryAdd(username.ToUpper(), 1);
        }

        public static void RemoveUser(string username)
        {
            UsersToTrack.TryRemove(username.ToUpper(), out int _);
        }

        static UserDataTracking()
        {
            string FullUserDataTrackingPath = Path.GetFullPath(Globals.Opts.UserDataTrackingPath);
            if (Directory.Exists(FullUserDataTrackingPath))
            {
                Directory.Delete(FullUserDataTrackingPath, true);
            }

            Directory.CreateDirectory(FullUserDataTrackingPath);
        }

        private static bool ShouldTrack(this NancyContext context)
        {
            var RequestMethod = context.Request.Method.ToUpper();
            if (RequestMethod == "POST" || RequestMethod == "PUT" || RequestMethod == "DELETE")
            {
                var RequestPath = context.Request.Path.ToUpper();
                if (RequestPath.StartsWith("/PRODUCTION") || RequestPath.StartsWith("/SITES"))
                {
                    var UserName = context.Request.GetUserName();
                    return UserName != null && UsersToTrack.ContainsKey(UserName);
                }
            }

            return false;
        }

        private static string GetTrackingDirectory(string UserName, long TrackingId)
        {
            var TrackingDir = Path.Combine(Globals.Opts.UserDataTrackingPath, UserName, $"{TrackingId}");
            Directory.CreateDirectory(TrackingDir);

            return TrackingDir;
        }

        private static void StoreRequest(string UserName, long TrackingId, Request Request)
        {
            var RequestPath = Path.Combine(GetTrackingDirectory(UserName, TrackingId), "Request.json");

            string requestBody = RequestStream.FromStream(Request.Body).AsString();
            File.WriteAllText(RequestPath, requestBody);
        }

        private static void StoreSites(string UserName, long TrackingId, string Prefix)
        {
            var DataPath = Path.Combine(GetTrackingDirectory(UserName, TrackingId), $"SITES-{Prefix}.json");
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var Sites = DB.Sites
                    .AsNoTracking()
                    .Include(s => s.Buildings)
                        .ThenInclude(b => b.ReclaimableMaterials)
                    .Include(s => s.Buildings)
                        .ThenInclude(b => b.RepairMaterials)
                    .Where(s => s.UserNameSubmitted == UserName)
                    .AsSplitQuery()
                    .ToList();

                File.WriteAllText(DataPath, JsonConvert.SerializeObject(Sites, Formatting.Indented));
            }
        }

        private static void StoreProduction(string UserName, long TrackingId, string Prefix)
        {
            var DataPath = Path.Combine(GetTrackingDirectory(UserName, TrackingId), $"PRODUCTION-{Prefix}.json");
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var ProductionLines = DB.ProductionLines
                    .AsNoTracking()
                    .Include(pl => pl.Orders)
                        .ThenInclude(o => o.Inputs)
                    .Include(pl => pl.Orders)
                        .ThenInclude(o => o.Outputs)
                    .Where(s => s.UserNameSubmitted == UserName)
                    .AsSplitQuery()
                    .ToList();

                File.WriteAllText(DataPath, JsonConvert.SerializeObject(ProductionLines, Formatting.Indented));
            }
        }

        static ConcurrentDictionary<Request, long> PendingRequests = new ConcurrentDictionary<Request, long>();

        static long TrackingIdCounter = 0;
        public static Func<NancyContext, Response> BeforeRequest()
        {
            return context =>
            {
                if (context.ShouldTrack())
                {
                    var RequestPath = context.Request.Path.ToUpper();
                    long TrackingId = Interlocked.Increment(ref TrackingIdCounter);
                    PendingRequests.TryAdd(context.Request, TrackingId);

                    StoreRequest(context.Request.GetUserName(), TrackingId, context.Request);
                    
                    if (RequestPath.StartsWith("/PRODUCTION"))
                    {
                        StoreProduction(context.Request.GetUserName(), TrackingId, "Before");
                    }
                    else if (RequestPath.StartsWith("/SITES"))
                    {
                        StoreSites(context.Request.GetUserName(), TrackingId, "Before");
                    }
                }

                return null;
            };
        }

        public static Action<NancyContext> AfterRequest()
        {
            return context =>
            {
                long TrackingId;
                if (context.ShouldTrack() && PendingRequests.TryGetValue(context.Request, out TrackingId))
                {
                    var RequestPath = context.Request.Path.ToUpper();
                    if (RequestPath.StartsWith("/PRODUCTION"))
                    {
                        StoreProduction(context.Request.GetUserName(), TrackingId, "After");
                    }
                    else if(RequestPath.StartsWith("/SITES"))
                    {
                        StoreSites(context.Request.GetUserName(), TrackingId, "After");
                    }
                }
            };
        }
    }
}
